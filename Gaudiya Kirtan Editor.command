
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR

nodeInstalled=$(which node)
brewInstalled=$(which brew)

if [ "$nodeInstalled" == "" ]; then
  clear
  echo -e "\033[91m\n  ~~~ PRESS CTRL+C AT ANY TIME TO EXIT ~~~\033[39m\n"
  echo -ne "\033[32m  Can I install nodejs first [y/n]? \033[39m" ; read yn ; echo
  if [ "$yn" == "y" ]; then
    if [ "$brewInstalled" == "" ]; then
      echo -ne "\033[32m  Can I install brew (so I can install node) first [y/n]? \033[39m"; read yn; echo
      if [ "$yn" == "y" ]; then
        echo "/usr/bin/ruby -e \"\$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)\""
      else
        exit
      fi
    fi
    echo brew install node
  else
    exit
  fi
fi

cd editor

clear
echo -e "\033[91m\n  ~~~ PRESS CTRL+C AT ANY TIME TO EXIT ~~~\033[39m\n"
node server.js >> ../json-bak/log.txt
