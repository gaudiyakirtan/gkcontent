
var fs = require("fs");
var pr = require("../editor/pretty.js");
var md5 = require('md5');    // npm install md5
var dataFolder = "../json/";

var list=[];
var song;

// list = fs.readFileSync(`${dataFolder}_list.json`, 'utf8');
// list = JSON.parse(list.toString());

fs.readdirSync(dataFolder).forEach(file => {
  if(file.match(/^[A-Z]{1,4}\d{1,3}.json/))
    list.push({uid:file.replace(".json","")});
});


var ll = list.length;
console.log(ll);
for(var s=0; s<ll; s++){
  song = fs.readFileSync(`${dataFolder}${list[s].uid}.json`, 'utf8');
  song_md5 = md5(song);

  try{
    song = JSON.parse(song);

    console.log(song.uid);

    list[s]={
      uid:list[s].uid,
      md5:song_md5,
      audio:!!song.audio.length,
      language:song.language,
      title:pr.cleanTitle(song.title),
      author:pr.cleanTitle(song.author)
    };
  } catch(e){ console.log("JSON.parse failed on:",list[s]); }
}

// letter Heading (for sort order)
var lh = ["M","G","GV","V","PT","N","GH","GN","GP","R","RK","K","B","NK","NM","SI","U","MS","S","Y","RBD","PBC","UP","BS","A","E","PR","ASTA","D","P","VT","SB","GG","AS","O","X"];
var listOutputString = pr.prettyList(JSON.stringify(list.sort(naturalSort)));
fs.writeFileSync(`${dataFolder}_list.json`, listOutputString );
fs.writeFileSync(`${dataFolder}_list.md5`, md5(listOutputString) );

function naturalSort(a,b){
  if(b.uid=="A0") return 1;
  // sort by letter and natural number, rather than strict alphabetical
  aU = a.uid.match(/[A-Z]{1,4}/)[0];
  bU = b.uid.match(/[A-Z]{1,4}/)[0];
  aN = a.uid.replace(/[A-Z]{1,4}/,"")*1;
  bN = b.uid.replace(/[A-Z]{1,4}/,"")*1;

  if(aU != bU) return lh.indexOf(aU) > lh.indexOf(bU) ? 1:-1;
  return aN > bN ? 1:-1;
}
