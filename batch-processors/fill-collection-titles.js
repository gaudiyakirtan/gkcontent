var fs = require("fs");
var pr = require("../editor/pretty.js");
var dataFolder = "../json/";

var list=[];
var song;

fs.readdirSync(dataFolder).forEach(file => {
  if(file.match(/^c.[A-Z]{1,}.*.json/))
    list.push({uid:file.replace(".json","")});
});

// fn="";
// output="";
for(var s=0; s<list.length; s++){
  l=list[s];
  f = fs.readFileSync(`${dataFolder}${l.uid}.json`, 'utf8');

  try{
    dat = JSON.parse(f);
    for(var i in dat){
      if(dat[i].type=="song"){
        song = fs.readFileSync(`${dataFolder}${dat[i].uid}.json`, 'utf8');
        song = JSON.parse(song);
        console.log(song.title);
        dat[i].title = song.title;
      }
    }

    var fOutputString = pr.prettyList(JSON.stringify(dat));
    fs.writeFileSync(`${dataFolder}${l.uid}.json`, fOutputString );
  }

  catch(e){}
}
