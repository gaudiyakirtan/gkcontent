// automatically updates:
//   "has_synonyms"
//   "has_translations"


var fs = require("fs");
var pr = require("../editor/pretty.js");
var dataFolder = "../json/";

var list;
var song;
var language="eng";

list = fs.readFileSync(`${dataFolder}_list.json`, 'utf8');
list = JSON.parse(list.toString());

pass=true;
for(var i=0; i<list.length; i++){
  song = fs.readFileSync(`${dataFolder}${list[i].uid}.json`, 'utf8');
  song = JSON.parse(song);
  tran = "UNDEFINED";
  syno = "NULL";
  bang = false;
  // song.has_synonyms
  for(var v=0; v<song.verses.length; v++){
    try{ tran=song.verses[v].translation[language]; }catch(e){
      // console.log('listIndex:',i,`, uid:${song.uid} , verse`,v,"doesn't exist");
      bang=true;
    }
    if(tran!="UNDEFINED") song.has_translations = true;
    // console.log(tran);
    for(var l=0; l<song.verses[v].lines.length; l++){
      for(var w=0; w<song.verses[v].lines[l].length; w++){
        syno=song.verses[v].lines[l][w].s[language];
        if(syno!="NULL") song.has_synonyms = true;
        // console.log(syno);
      }
    }
  }
  diff=song.has_synonyms!=song.has_translations;
  fs.writeFileSync(`${dataFolder}${song.uid}.json`, pr.pretty(JSON.stringify(song)) );
  console.log(bang?'!':' ',diff?'*':' ','listIndex:',i,`, uid:${song.uid} , has_synonyms:${song.has_synonyms} , has_translations:${song.has_translations}`);
}
console.log("! = at least one verse is empty\n * = has one but not the other synonyms/translations");
