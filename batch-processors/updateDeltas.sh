md5_A=$(md5 -q data.json)
node build-delta-file.js
md5_B=$(md5 -q data.json)

cp bundle.md5 ../songs/
cp delta.json ../songs/

if [[ $md5_A != $md5_B ]]; then
  echo
  echo data.json was updated...
  echo
  node compress.js
  cp datalz.ts ~/+/gaudiyakirtan/ux-app/src/components/screens/shared/song/
fi
