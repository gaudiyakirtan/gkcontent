const versionNumber = 3;

// ==== LZW COMPRESSION :
// var FILE = "data.json";
var FILE = "data.json";
var FILE_OUT = "data.lz";
var FILE_TS = "datalz.ts";

if(process.argv[2]){
  FILE = process.argv[2];
  FILE_OUT = process.argv[2].replace("json", "lz");
  FILE_TS = process.argv[2].replace(".json", "lz.ts");
}
// process.exit();

// LZW-compress a string
function lzw_encode(s) {
  var dict = {};
  var data = (s + "").split("");
  var out = [];
  var currChar;
  var phrase = data[0];
  var code = 57344;
  for (var i = 1; i < data.length; i++) {
    currChar = data[i];
    if (dict[phrase + currChar] != null) {
      phrase += currChar;
    } else {
      out.push(phrase.length > 1 ? dict[phrase] : phrase.charCodeAt(0));
      dict[phrase + currChar] = code;
      code++;
      phrase = currChar;
    }
  }
  out.push(phrase.length > 1 ? dict[phrase] : phrase.charCodeAt(0));

  return out;
}

// ==== COMPRESS SONG & LIST

var fs = require("fs");

if(!FILE){
  var dataFolder = "../json/";
  var list;

  listRaw = fs.readFileSync(`${dataFolder}_list.json`, "utf8");
  list = JSON.parse(listRaw.toString());

  output = [];
  // output.push({ list: listRaw });
  for (var s = 0; s < list.length; s++) {
    song = fs.readFileSync(`${dataFolder}${list[s].uid}.json`, "utf8");
    output.push({ uid: list[s].uid, song: song });
  }

  var data = JSON.stringify(output);
  var comp = byteArray(lzw_encode(data));
}
else{
  var data = fs.readFileSync(FILE, "utf8");
  var comp = byteArray(lzw_encode(data));
}

function byteArray(array){
  var x, a, b, c;
  var w = "";
  var maxByte = 0;
  console.log(array.slice(0,64));
  for (var i = 0; i < array.length; i++) {
    x = array[i];
    a = x >> 16; x -= (a * 1) << 16;
    b = x >> 8;  x -= (b * 1) << 8;
    c = x;

    w += String.fromCharCode(a);
    w += String.fromCharCode(b);
    w += String.fromCharCode(c);
    if (array[i] > maxByte) maxByte = array[i];
  }
  console.log("max < 3 bytes?", maxByte >> 16 < 256);
  return w;
}

b64= Buffer.from(comp).toString('base64');
if(!FILE_OUT) fs.writeFileSync(`${dataFolder}data.lz`, b64);
else{
  fs.writeFileSync(FILE_OUT, b64);
  fs.writeFileSync(FILE_TS,
    "// @ts-nocheck"
    + "\nexport default function fetchData(versionOnly = false){"
    + "\nif (versionOnly) return version;"
    + "\n  return data;"
    + "\n}"
    + `\nconst version = "${versionNumber}";`
    + "\nlet data = `" + b64 + "`;");
}

console.log("uncompressed bytes", data.length);
console.log("compressed bytes", comp.length);
console.log("compressed b64 bytes", b64.length);

console.log(data.substr(-108));
