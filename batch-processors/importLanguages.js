
var fs = require("fs");
var pr = require("../editor/pretty.js");
var dataFolder = "../json/";

var list = [];
var song;
var songString;

fs.readdirSync(dataFolder).forEach(file => {
  if(file.match(/^[A-Z]{1,4}\d{1,3}.json/))
    list.push({uid:file.replace(".json","")});
});

lang = fs.readFileSync('../archive/language.json', 'utf8');
lang = JSON.parse(lang.toString());

for(var s=0; s<list.length; s++){
  song = fs.readFileSync(`${dataFolder}${list[s].uid}.json`, 'utf8');
  song = JSON.parse(song);

  if(!song.language || song.language=="?"){
    // console.log(song.uid, lang[song.uid]);
    //
    // if(song.uid!=lang[s].uid){
    //   console.log(`mismatch s:${s} song.uid:${song.uid} lang.uid:${lang[s].uid}`);
    //   process.exit;
    // }
    song.language=lang[song.uid];
    console.log(`saving: ${dataFolder}${list[s].uid}.json`);
    songString=pr.pretty(JSON.stringify(song));
    fs.writeFileSync(`${dataFolder}${list[s].uid}.json`, songString );
  }
}
