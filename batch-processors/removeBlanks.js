// automatically updates:
//   "has_synonyms"
//   "has_translations"


var fs = require("fs");
var pr = require("../editor/pretty.js");
var dataFolder = "../json/";

var list;
var song;
var language="eng";

list = fs.readFileSync(`${dataFolder}_list.json`, 'utf8');
list = JSON.parse(list.toString());

pass=true;
// s=35;
for(var s=0; s<list.length; s++){
  song = fs.readFileSync(`${dataFolder}${list[s].uid}.json`, 'utf8');
  song = JSON.parse(song);

  vlen=song.verses.length;
  for(var v=0; v<vlen; v++){
    verse_empty = true;
    for(var l=0; l<song.verses[v].lines.length; l++){
      line_empty = true;
      for(var w=0; w<song.verses[v].lines[l].length; w++){
        wrd = song.verses[v].lines[l][w].w;
        if(wrd!="x") line_empty=false;
        else delete song.verses[v].lines[l][w];
      }
      if(line_empty) console.log(s,v,l,song.verses[v].lines[l]);
      if(line_empty) delete song.verses[v].lines[l];
      else verse_empty=false;
    }
    if(verse_empty) delete song.verses[v];
  }
  var output = pr.pretty(JSON.stringify(song).replace(/null,/g, "").replace(/,null/g, ""));
  console.log('listIndex:',s,`, uid:${song.uid} , saved.`);
  fs.writeFileSync(`${dataFolder}${song.uid}.json`, output );
}
