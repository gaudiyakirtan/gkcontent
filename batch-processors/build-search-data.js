
var convert = require("../editor/convert.js");
var fuzz = require("./fuzzy.js");
var pr = require("../editor/pretty.js");
var fs = require("fs");

var dataFolder = "../json/";

var list;
var searchIndex;
var song;

list = fs.readFileSync(`${dataFolder}_list.json`, 'utf8');
list = JSON.parse(list.toString());

// PUT THIS SECTION BACK IN WHEN YOU'RE CHECKING MD5's
/*
try{
  searchIndex = fs.readFileSync(`${dataFolder}search.json`, 'utf8');
  searchIndex = JSON.parse(searchIndex.toString());
}catch(e){
  console.log("No search.json file found. Creating a fresh one.");
  searchIndex = [];
}
*/

searchIndex=[];

for(var s=0; s<list.length; s++) addEntry(list[s].uid);

function addEntry(uid){
  console.log("Building entry for",uid);
  song = fs.readFileSync(`${dataFolder}${uid}.json`, 'utf8');
  song = JSON.parse(song);
  var output="";
  for(var v=0; v<song.verses.length; v++){
    for(var l=0; l<song.verses[v].lines.length; l++){
      for(var w=0; w<song.verses[v].lines[l].length; w++){
        wrd = song.verses[v].lines[l][w].w;
        output+=convert.transliterate(wrd, song.language);
      }
      output+=`{${v}|${l}}`;
    }
  }
  // t=fuzz.unNum(output);
  // t=fuzz.unPunc(t);
  t=fuzz.unPunc(output);
  t=fuzz.unDiac(t);
  t=fuzz.unDiac(t);
  t=fuzz.deDbl(t);
  t=fuzz.similar(t);
  t=fuzz.deAspirate(t);
  t=fuzz.deNasal(t);
  t=fuzz.deVowel(t);
  output=fuzz.deDbl(t);

  titleRoma=convert.transliterate(song.title, song.language);
  t=fuzz.unNum(titleRoma);
  t=fuzz.unPunc(t);
  t=fuzz.unDiac(t);
  t=fuzz.deDbl(t);
  t=fuzz.similar(t);
  t=fuzz.deAspirate(t);
  t=fuzz.deNasal(t);
  t=fuzz.deVowel(t);
  title=fuzz.deDbl(t);

  authorBang=decodeAuthor(song.author);
  authorRoma=convert.transliterate(authorBang, song.languageg);
  t=fuzz.unNum(authorRoma);
  t=fuzz.unPunc(t);
  t=fuzz.unDiac(t);
  t=fuzz.deDbl(t);
  t=fuzz.similar(t);
  t=fuzz.deAspirate(t);
  t=fuzz.deNasal(t);
  t=fuzz.deVowel(t);
  author=fuzz.deDbl(t);

  // TODO: do multiple fuzz levels instead of bang & roma.
  searchIndex.push({
    uid:song.uid,
    titleFuzz:title,
    titleBang:song.title,
    titleRoma:titleRoma,
    authorFuzz:author,
    authorBang:authorBang,
    authorRoma:authorRoma,
    content:output
  });
}

fs.writeFileSync(`${dataFolder}search.json`, pr.prettyList(JSON.stringify(searchIndex)));


function decodeAuthor(s){
  return s
    .replace("bt", "শ্রীল ভক্তিবিনোদ ঠাকুর")
    .replace("ndt", "শ্রীল নরোত্তম দাস ঠাকুর")
    .replace("ldt", "শ্রীল লোচন দাস ঠাকুর")
    .replace("vct", "শ্রীল বিশ্বনাথ চক্রবর্তী ঠাকুর")
    .replace("vdt", "শ্রীল বৃন্দাবন দাস ঠাকুর")
    .replace("kkg", "শ্রীল কৃষ্ণদাস কবিরাজ গোস্বামী")
    .replace("bsst", "শ্রীল ভক্তিসিদ্ধান্ত সরস্বতী গোস্বামী প্রভুপাদ")
    .replace("rg", "শ্রীল রূপ গোস্বামী")
    .replace("rdg", "শ্রীল রঘুনাথ দাস গোস্বামী")
    .replace("pdt", "শ্রীল প্রেমানন্দ দাস ঠাকুর");
}
