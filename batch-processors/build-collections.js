var fs = require("fs");
var dataFolder = "../collections/";
var list={children:[]};

function load(uid){
  data = fs.readFileSync(`${dataFolder}${uid}.col`, 'utf8');
  data = data.split("\n");
  if(data[data.length-1]=="") data.pop();
  return data;
}

list.children = load("list");
for(var i in list.children){
  list.children[i]={
    type:list.children[i].split(":")[0] !== "*" ? "collection" : "heaading",
    uid:list.children[i].split(":")[0],
    title:list.children[i].split(":")[1],
  };
}

var count;
render(list.children, 0);
for(var i in list.children){
  count=0;
  if (list.children[i].type === "collection"){
    append(`0,${i}`);
    list.children[i].count=count;
    console.log(list.children[i].uid, count);
  }
}
fs.writeFileSync("collections.json", uniqueify(JSON.stringify(list)));
// console.log(uniqueify(JSON.stringify(list,null,2)));
// console.log(list);

function uniqueify(s){
  var i=0;
  return s.replace(/("uid":".{1,5})(",)/g, function(m,a,b){ return `${a}@${i++}${b}`; });
}

function render(obj, a){
  for(var i in obj){
    render(obj[i].children, `${a},${i}`);
  }
}

function append(address){
  a = address.split(",");
  obj = list;
  for(var i=1; i<a.length; i++){
    obj = obj.children[a[i]];
    if(!obj.children) populate(obj, load(obj.uid));
    // if(i==a.length-1) obj.open=!obj.open;
  }
  render(list.children, 0);
}

function populate(obj, lines){
  // obj.open=true;

  var ind=0;
  var oind=0;
  var newObj = ["["];

  function closeBracket(comma){
    oind=ind;
    if(!comma) ind=0;
    else ind=(lines[i][0]=="\t") + (lines[i][1]=="\t");
    for(var j=0; j<=1; j++){
      if(ind<oind-j){
        last=newObj.length-1;
        newObj[last]=newObj[last].replace(/,$/,'');
        newObj.push(`${"".padStart(oind*2-j*2," ")}]}${comma}`);
      }
    }
  }

  for(var i in lines){
    l=lines[i].replace(/\t/g,"");
    // console.log(l);
    if(l[0]=="*") type = "heading";
    else type = isNaN(l[0]) ? "song":"collection";
    if(type=="song") count++;
    ls = l.split(":");
    uid = ls[0];
    title = ls[1];
    closeBracket(",");
    if(type=="collection"){ c=0; children=', "children":['; }
    else children="},";
    newObj.push(`${"".padStart(ind*2+2," ")}{"type":"${type}", "uid":"${uid}", "title":"${title}"${children}`);
  }
  closeBracket("");
  last=newObj.length-1;
  newObj[last]=newObj[last].replace(/,$/,'');
  newObj.push("]");
  // console.log(newObj.join("\n"));
  try{ obj.children=JSON.parse(newObj.join("\n")); }
  catch(e){
    console.log(newObj.join("\n"));
    console.error(e);
    process.exit();
  }
}
