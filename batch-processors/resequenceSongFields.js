var fs = require("fs");
var pr = require("../editor/pretty.js");
var dataFolder = "../json/";

var list;
var song;

list = fs.readFileSync(`${dataFolder}_list.json`, 'utf8');
list = JSON.parse(list.toString());

for(var s=0; s<list.length; s++){
  song = fs.readFileSync(`${dataFolder}${list[s].uid}.json`, 'utf8');
  song = JSON.parse(song);

  song={
    // uid:song.uid,    // preserve uid from song data itself
    uid:list[s].uid, // force uids to match song filename
    title:song.title || "?",
    author:song.author || "?",
    language:song.language || "?",
    notes:song.notes || [],
    audio:song.audio || [],
    has_synonyms:song.has_synonyms,
    has_translations:song.has_translations,
    legend:song.legend,
    verses:song.verses
  };

  var output = pr.pretty(JSON.stringify(song));
  console.log('listIndex:',s,`, uid:${song.uid} , saved.`);
  fs.writeFileSync(`${dataFolder}${song.uid}.json`, output );
}
