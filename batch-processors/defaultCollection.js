var fs = require("fs");
var dataFolder = "../json/";

var list;

list = fs.readFileSync(`${dataFolder}_list.json`, 'utf8');
list = JSON.parse(list.toString());

clist = fs.readFileSync("../collections/all.categories.coldat", 'utf8');
clist = clist.split("\n");

let heading = {};
for(var s=0; s<clist.length; s++){
  let parts = clist[s].split(":");
  heading[parts[0]] = parts[1];
}

fn="";
output=`1:Help\n\t${list[0].uid}:${list[0].title}`;
previousSection = "";
c=2;
for(var s=1; s<list.length; s++){
  l=list[s];
  let section = l.uid.match(/[A-Z]{1,4}/)[0];
  if (section!==previousSection){
    output+=`\n${c++}:${section} - ${heading[section]}`;
    previousSection = section;
  }
  entry=`\n\t${l.uid}:${l.title}`;
  output+=entry;
}
console.log(output);
fs.writeFileSync("../collections/ALL.col", output);
