var fs = require("fs");
var songFolder = "../json/";
var colFile = process.argv.pop();
var commit = false;

if (colFile==="commit"){
  commit = true;
  colFile = process.argv.pop();
}

var list,song;

try{ list = fs.readFileSync(colFile, 'utf8').split("\n"); }
catch(e){ console.log(`Couldn't open "${colFile}"`); process.exit(); }

for(var s=0; s<list.length; s++){
  l=list[s].replace(/\t/g,"");

  if(!l.match(/^\s*[A-Z]{1,4}\d{1,3}$/)){
    // list[s] = list[s].replace(" ", ":");
    continue;
  }

  song = JSON.parse(fs.readFileSync(`${songFolder}${l.trim()}.json`));
  list[s] = list[s].replace(l, `${l}:${song.title}`);
}

if (commit) fs.writeFileSync(colFile, list.join("\n"));
else console.log(list.join("\n"));
