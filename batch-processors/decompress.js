// ==== LZW DECOMPRESSION :

function lzw_decode(data) {
  var dict = {};
  var currChar = String.fromCharCode(data[0]);
  var oldPhrase = currChar;
  var out = [currChar];
  var code = 57344;
  var phrase;
  for (var i = 1; i < data.length; i++) {
    var currCode = data[i];
    if (currCode < 57344) {
      phrase = String.fromCharCode(data[i]);
    } else {
      phrase = dict[currCode] ? dict[currCode] : oldPhrase + currChar;
    }
    out += phrase;
    currChar = phrase[0];
    dict[code] = oldPhrase + currChar;
    code++;
    oldPhrase = phrase;
  }
  return out;
}

// ==== CONVERT TO ARRAY & DECOMPRESS

function arrayFromBytes(bytes){
  var x, a, b, c;
  var array = [];
  for (var i = 0; i < bytes.length; i+=3) {
    a = bytes[i+0].charCodeAt();
    b = bytes[i+1].charCodeAt();
    c = bytes[i+2].charCodeAt();
    x = a * (1<<16) + b * (1<<8) + c;
    array.push(x);
  }
  console.log(array.slice(0,64));
  return array;
}

// export default
function decompress(string){
  comp = arrayFromBytes(string);
  return lzw_decode(comp);
}

var fs = require("fs");
var dataFolder = "../json/";
var comp;

var string = fs.readFileSync(`${dataFolder}data.lz`, "utf8");
console.log(string.substr(-108));
string = Buffer.from(string, 'base64').toString();
console.log(string.substr(-108));
var data = decompress(string);
console.log(data.substr(-108), data.length);
