var fs = require("fs");
var dataFolder = "../json/";

var list;
var data;
var pass;

list = fs.readFileSync(`${dataFolder}_list.json`, 'utf8');
list = JSON.parse(list.toString());

pass=true;
for(var i=0; i<list.length; i++){
  data = fs.readFileSync(`${dataFolder}${list[i].uid}.json`, 'utf8');
  try{ data = JSON.parse(data); }catch(e){ pass=false;
    console.log(`FAILED - listIndex:${i}, uid:${list[i].uid} ==> ${e}`); }
}
if(pass) console.log("Everything passed.");
