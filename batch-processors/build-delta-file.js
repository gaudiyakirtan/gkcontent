
var convert = require("../editor/convert.js");
var fuzz = require("./fuzzy.js");
var md5 = require('md5');    // npm install md5
var fs = require("fs");

var dataFolder = "../json/";

var list=[];
var song, songRaw;

fs.readdirSync(dataFolder).forEach(file => {
  if(file.match(/^[A-Z]{1,4}\d{1,3}.json/))
    list.push({uid:file.replace(".json","")});
});

// letter Heading (for sort order)
var lh = ["M","G","GV","V","PT","N","GH","GN","GP","R","RK","K","B","NK","NM","SI","U","MS","S","Y","RBD","PBC","UP","BS","A","E","PR","ASTA","D","P","VT","SB","GG","AS","O","X"];
list = list.sort(naturalSort);

function naturalSort(a,b){
  if(b.uid=="A0") return 1;
  // sort by letter and natural number, rather than strict alphabetical
  aU = a.uid.match(/[A-Z]{1,2}/)[0];
  bU = b.uid.match(/[A-Z]{1,2}/)[0];
  aN = a.uid.replace(/[A-Z]{1,2}/,"")*1;
  bN = b.uid.replace(/[A-Z]{1,2}/,"")*1;

  if(aU != bU) return lh.indexOf(aU) > lh.indexOf(bU) ? 1:-1;
  return aN > bN ? 1:-1;
}

var fresh = false;

// PUT THIS SECTION BACK IN WHEN YOU'RE CHECKING MD5's
try{
  searchIndex = fs.readFileSync(`data.json`, 'utf8');
  searchIndex = JSON.parse(searchIndex.toString());
  searchIndexDelta = {};
}catch(e){
  fresh = true;
  searchIndex = {};
  searchIndexDelta = {};
}

for(var s=0; s<list.length; s++) addEntry(list[s].uid);

function addEntry(uid){
  songRaw = fs.readFileSync(`${dataFolder}${uid}.json`, 'utf8');
  song = JSON.parse(songRaw);
  // console.log(md5(songRaw), searchIndex[uid].md5, uid);
  if(searchIndex[uid])
    if(md5(songRaw) == searchIndex[uid].md5) return;
  console.log("Building entry for", uid);
  var output="";
  for(var v=0; v<song.verses.length; v++){
    for(var l=0; l<song.verses[v].lines.length; l++){
      for(var w=0; w<song.verses[v].lines[l].length; w++){
        wrd = song.verses[v].lines[l][w].w;
        output+=convert.transliterate(wrd, song.language);
      }
      output+=`{${v}|${l}}`;
    }
  }
  // t=fuzz.unNum(output);
  // t=fuzz.unPunc(t);
  t=output.replace(/(ḥ|h\W)/g, "");
  t=fuzz.unPunc(t);
  t=fuzz.unDiac(t);
  t=fuzz.unDiac(t);
  t=fuzz.deDbl(t);
  t=fuzz.similar(t);
  t=fuzz.deAspirate(t);
  t=fuzz.deNasal(t);
  t=fuzz.deVowel(t);
  output=fuzz.deDbl(t);

  titleRoma=convert.transliterate(song.title, song.language);
  t=titleRoma.replace(/(ḥ|h\W)/g, "");
  t=fuzz.unNum(t);
  t=fuzz.unPunc(t);
  t=fuzz.unDiac(t);
  t=fuzz.deDbl(t);
  t=fuzz.similar(t);
  t=fuzz.deAspirate(t);
  t=fuzz.deNasal(t);
  t=fuzz.deVowel(t);
  title=fuzz.deDbl(t);

  authorBang=decodeAuthor(song.author);
  authorRoma=convert.transliterate(authorBang, song.languageg);
  t=authorRoma.replace(/(ḥ|h\W)/g, "");
  t=fuzz.unNum(t);
  t=fuzz.unPunc(t);
  t=fuzz.unDiac(t);
  t=fuzz.deDbl(t);
  t=fuzz.similar(t);
  t=fuzz.deAspirate(t);
  t=fuzz.deNasal(t);
  t=fuzz.deVowel(t);
  author=fuzz.deDbl(t);

  // TODO: do multiple fuzz levels instead of bang & roma.
  searchIndex[song.uid]=
  searchIndexDelta[song.uid]={
    song:song,
    md5:md5(songRaw),
    titleFuzz:title,
    contentFuzz:output
  };
}

fs.writeFileSync("delta.json", JSON.stringify(searchIndexDelta));
fs.writeFileSync("bundle.md5", md5(JSON.stringify(searchIndex)));
if(fresh){
  console.log("No data.json file found. Creating a fresh one.");
  fs.writeFileSync("data.json", JSON.stringify(searchIndex)+"\n");
}


function decodeAuthor(s){
  return s
    .replace("bt", "শ্রীল ভক্তিবিনোদ ঠাকুর")
    .replace("ndt", "শ্রীল নরোত্তম দাস ঠাকুর")
    .replace("ldt", "শ্রীল লোচন দাস ঠাকুর")
    .replace("vct", "শ্রীল বিশ্বনাথ চক্রবর্তী ঠাকুর")
    .replace("vdt", "শ্রীল বৃন্দাবন দাস ঠাকুর")
    .replace("kkg", "শ্রীল কৃষ্ণদাস কবিরাজ গোস্বামী")
    .replace("bsst", "শ্রীল ভক্তিসিদ্ধান্ত সরস্বতী গোস্বামী প্রভুপাদ")
    .replace("rg", "শ্রীল রূপ গোস্বামী")
    .replace("rdg", "শ্রীল রঘুনাথ দাস গোস্বামী")
    .replace("pdt", "শ্রীল প্রেমানন্দ দাস ঠাকুর");
}
