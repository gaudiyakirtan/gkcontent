// s=deDbl(deVowel(deNasal(deAspirate(similar(deDbl(unPunc(unDiac(s))))))));

function unDiac(s){
  re = new RegExp(String.fromCharCode(8204), "g");
  return s
    .replace(re, "")
    .replace(/ã|ā̃|ā/g, "a")
    .replace(/ĩ|ī̃|ī/g, "i")
    .replace(/ũ|ū̃|ū/g, "u")

    .replace(/ẽ/g, "e")
    .replace(/õ/g, "o")

    .replace(/ṛ|ṝ/g, "r")
    .replace(/ḷ|ḹ/g, "l")

    .replace(/ḍ|ḓ|ɽ/g, "d")
    .replace(/ṅ|ṇ|ñ/g, "n")
    .replace(/ś|ṣ/g, "s")
    .replace(/ṁ/g, "m")
    .replace(/ḥ/g, "h")
    .replace(/ṭ/g, "t")
    .replace(/ẏ/g, "y")
    .replace(/·/g, "");
}

function unNum(s){
  return s.replace(/[0-9]/g,'');
}

function unPunc(s){
  return s.replace(/\s|\\|\-|\!|,|\.|\(|\)|’|~|\?|‘/g,'');
}

function deDbl(s){
  return s.replace(/(.)\1/g, '$1');
  // thissis a doubble bubble wordd yes.
}

function similar(s){
  return s
    .replace(/ia/g, 'ya')
    .replace(/w/g, 'b')
    .replace(/v/g, 'b')
    .replace(/r/g, 'd')
    .replace(/y/g, 'j')
    .replace(/o/g, 'a');
}

function deAspirate(s){
  return s.replace(/(s|c|k|g|j|d|t|c|b|p)h/g, '$1');
  // kha gha jha dha tha cha bha pha => ka ga ...
}

function deNasal(s){
  return s.replace(/n(k|g|g|c|j|t|d)/g, '$1');
  // kandiya kandiya ==> kadiya kadiya
}

function deVowel(s){
  return s.replace(/a|e|i|o|u/g, '');
}

function fuzzyAll(s){
  return deDbl(deVowel(deNasal(deAspirate(similar(deDbl(unPunc(unDiac(s))))))));
}
module.exports = { unDiac,unNum,unPunc,deDbl,similar,deAspirate,deNasal,deVowel,fuzzyAll };
