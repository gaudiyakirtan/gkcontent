var updateJSON = !false;
var gkaudio = "../../gkaudio/";

var pr = require("../editor/pretty.js");
var fs = require("fs");
var list,uid,song;
var prev_uid=false;
var lookup={};

// list = fs.readFileSync('ls.json', 'utf8');
// list = list.split("\n");

// this has all the same info as the ls.json.
// NEEDS TESTING !!!!!!!!
list = fs.readFileSync('choice-audio.json', 'utf8');
list = JSON.parse(list);

uid = fs.readFileSync('uid2audiofile.json', 'utf8');
uid = uid.split("\n");

for(var i=0; i<uid.length; i++){
  group = uid[i].split("\t");
  lookup[group[1]]=group[0];
}

setCodes();

var counter = {};
for(var s=0; s<list.length; s++){
  var raw=list[s].src;
  var p=raw.search(/\/(?:.(?!\/))+$/);
  if(p!=-1) raw = raw.substr(p);

  src=raw.replace("/","")
    .replace(/^[a-z]{4}\-/,"")
    .replace("????-","")
    .replace(" copy","")
    // .replace(/_/g, " ")
    .replace(/(.mp3|.m4a)/,"")
    .replace(/\-\d/,"");

  targ_uid=lookup[src]; if(targ_uid=="_") continue;

  // write to file if the uid changes
  // if(targ_uid!=prev_uid && prev_uid && updateJSON){
  //   for(var i=0; i<song.audio.length; i++){
  //     console.log(song.audio[i]);
  //   }
  //
  // }
    // console.log(song);
  //   fs.writeFileSync(`../gkcontent/json/${prev_uid}.json`, pr.pretty(JSON.stringify(song)));

  code = raw.replace(/.*(\/|^)([a-z]{4}|\?\?\?\?).*/, function(m,g1,g2){ return g2; });
  ext = raw.replace(/.*(mp3|m4a)$/, function(m,g1){ return g1; });

  dst = `${targ_uid}-${code}-%%.${ext}`;
  if(!counter[dst]) counter[dst] = 1;
  else counter[dst]++;
  ver = counter[dst];

  dst = dst.replace("%%",ver);

  if(targ_uid!=prev_uid){
    if(song){
       // console.log(song);
       fs.writeFileSync(`../json/${prev_uid}.json`, pr.pretty(JSON.stringify(song)));
       console.log(`cp "${gkaudio}${list[s].src}" "${gkaudio}audio-uids/${dst}"`);
    }

    song = fs.readFileSync(`../json/${targ_uid}.json`, 'utf8');
    song = JSON.parse(song);
    song.audio=[];
  }

  entry = {
    uid:`${code}-${ver}`,
    fn:dst,
    meta:{artist:codes[code],src:list[s].src}
  };
  if(list[s].priority==1) song.audio.unshift(entry);
  else song.audio.push(entry);
    // fs.writeFileSync(`${gkaudio}songs-uids/${targ_uid}.json`, pr.pretty(JSON.stringify(song)));
    // console.log(targ_uid);
    // if(targ_uid=="R20")
      // console.log(targ_uid,song.audio);
      // console.log(targ_uid,pr.pretty(JSON.stringify(song)));
  // }

  prev_uid = targ_uid;
}

var codes;
function setCodes(){
  codes={
    anad:"Anadi Krsna das",
    bppm:"Srila BP Puri Gosvami Maharaja",
    brsm:"Srila BR Sridhara Gosvami Maharaja",
    bvnm:"Srila BV Narayana Gosvami Maharaja",
    bvsm:"Srila BV Svami Prabhupada",
    bvtm:"Srila BV Trivikrama Gosvami Maharaj",
    casu:"Candrakanta & Sucandra",
    damo:"Damodar das",
    gaur:"Gaurasundar das",
    hakr:"Hare Krsna das",
    jahn:"Jahnava dasi",
    jana:"Janaki dasi",
    jays:"Jaya Sri dasi",
    kana:"Kanai Prabhu",
    kimo:"Kishori Mohan das",
    krsb:"Krsna das Babaji",
    krsn:"Krsnadas das",
    madh:"Madhukar das",
    maya:"Mayapur Gurukul",
    mith:"Mithu dasi",
    muni:"BV Muni Maharaja",
    nank:"Nanda Kishor das",
    pare:"Paresananda das",
    rash:"Radhesh das",
    rasi:"Rasika dasi",
    rauk:"Radhika dasi (UK)",
    raus:"Radhika dasi (Bay Area)",
    rpri:"Radha Priya dasi",
    sadh:"BV Sadhu Maharaja",
    sara:"Sarasvati dasi",
    sesa:"Sesasayi Prabhu",
    sggm:"Srila Gaura Govinda Gosvami Maharaja",
    shbi:"Shyam-bihari das",
    srav:"Sravan das",
    srrb:"Sudarsan das (Radha-ramana Babaji Maharaja)",
    srvi:"Sri Vidyabhusan",
    sude:"Sudevi dasi",
    sudf:"Sudarsan das (Florida)",
    suma:"Suman Bhattacarya",
    tama:"Tamal Krsna das",
    taru:"BV Tirtha Maharaja",
    tirt:"Tarun Krsna",
    vija:"Vijay das",
    vraj:"Vraj Mohan das",
    "????":"Unknown Artist"
  };
}
