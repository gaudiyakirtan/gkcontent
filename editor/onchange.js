
function uidChanged(){
  // move the element and call updateListOrder()
  console.log('uid changed');
}

// ========================================================================== //

function titleChanged(){
  var j = listOrder.indexOf(`${currentSong}`);
  var k = currentSong;
  currentSongList = songlist.getElementsByClassName("list-item");

  titleText = songtitle.innerHTML;
  title = `<span class=edited>${titleText}</span>
    <small>[${(listData[k].uid!=listData[k].new_uid ?
      `<s>${listData[k].uid}</s>&rarr;`
      :'')}${listData[k].new_uid}]</small>`;

  var changeMarker = ' <s>*</s>';
  if(scanSongChanges(currentSong)) title+=changeMarker;

  currentSongList[j].innerHTML = title;
  titleChanges[k] = titleText.replace(/,/g,"[comma]");
  localStorage.gkTitleChanges=titleChanges.join();
}
function scanSongChanges(n){
  for(var i in songChanges) if(i.split(',')[0]==n) return true;
  return false;
}

// ========================================================================== //

function authorChanged(){
  var j = listOrder.indexOf(`${currentSong}`);
  var k = currentSong;
  currentSongList = songlist.getElementsByClassName("list-item");

  authorText = songauthor.innerHTML;
  songauthor.setAttribute('class', 'headerField edited');

  authorChanges[k] = authorText.replace(/,/g,"[comma]");
  localStorage.gkAuthorChanges=authorChanges.join();
}

// ========================================================================== //

function notesChanged(){
  var j = listOrder.indexOf(`${currentSong}`);
  var k = currentSong;
  currentSongList = songlist.getElementsByClassName("list-item");

  notesText = songnotes.innerHTML;
  songnotes.setAttribute('class', 'headerField edited');

  notesChanges[k] = notesText.replace(/,/g,"[comma]");
  localStorage.gkNotesChanges=notesChanges.join();
}

// ========================================================================== //

function toggleMode(el, m){
  mode[m]=!mode[m];
  el.setAttribute('class', `tab button${mode[m]?' active':''}`);
  if(m=='roma'){
    songtitle.setAttribute('class',(mode[m]?'frozen':'') );
    songauthor.setAttribute('class',(mode[m]?'headerField frozen':'headerField') );
    songnotes.setAttribute('class',(mode[m]?'headerField frozen':'headerField') );
    songtitle.setAttribute('contenteditable', !mode[m]);
    songauthor.setAttribute('contenteditable', !mode[m]);
    songnotes.setAttribute('contenteditable', !mode[m]);
    generatelist();
  }
  localStorage.gkModes=JSON.stringify(mode);
  setTimeout(updateViewer,0,currentSong);
  // updateViewer(currentSong);
}

// ========================================================================== //

function updateListOrder(){
  listOrder=[];
  currentSongList = songlist.getElementsByClassName("list-item");
  for(var i=0; i<currentSongList.length; i++){
    listOrder.push(currentSongList[i].getAttribute('index'));
  }
  localStorage.gkListOrder = listOrder.join();
  // showsong(currentSong);

  // calling generatelist() here is super inefficient,
  // but it's easier than updating only the effected uid markers
  setTimeout(generatelist,0);
}

// ========================================================================== //

function markTitle(){
  activelistItem = songlist.getElementsByClassName("list-item")
    [listOrder.indexOf(`${currentSong}`)];

  var changeMarker = ' <s>*</s>';
  if(activelistItem.innerHTML.substr(-changeMarker.length)!=changeMarker)
    activelistItem.innerHTML+=changeMarker;
}

// ========================================================================== //

function tranFieldChange(n,v,el){
  el.setAttribute('class', `translation edited`);
  songChanges[`${n},${v},tran`] = { translation:el.innerHTML };
  localStorage.gkSongChanges=JSON.stringify(songChanges);
  markTitle();
}

function fieldChange(scKey,cssClass,key,el){
  var k=scKey.split(',');

  var effectiveText = el.innerHTML.replace(/\[|\]/g, "");
  if(effectiveText==songs[k[0]].verses[k[1]].lines[k[2]][k[3]][key]){
    el.setAttribute('class', `${cssClass}`);
    delete songChanges[scKey];
    localStorage.gkSongChanges=JSON.stringify(songChanges);
    return;
  }
  if(key=='s') if(el.innerHTML==songs[k[0]].verses[k[1]].lines[k[2]][k[3]][key][language]){
    el.setAttribute('class', `${cssClass}`);
    delete songChanges[scKey];
    localStorage.gkSongChanges=JSON.stringify(songChanges);
    return;
  }

  isRoman=el.parentElement.getAttribute('class')=='roma';
  el.setAttribute('class', `${cssClass} edited`);
  if(!songChanges[scKey]) songChanges[scKey]={};
  songChanges[scKey][key]=effectiveText;

  var verseElement = el.parentElement.parentElement;
  renderFunc=[renderBang,null,renderRoma,renderSyno,renderTran];
  // console.log(renderFunc);

  // var HTML="";
  var len=verseElement.children.length;
  for(var i=0; i<len; i++){
    if(i>7) return;
    thisClass = verseElement.children[i].getAttribute('class');
    if( verseElement.children[i]==el.parentElement ) continue;  // what was this for?

    renderFuncSelector=tabNames.indexOf(thisClass);
    if(renderFuncSelector==-1) continue; // null
    HTML=renderFunc[renderFuncSelector](k[0],k[1],false);
    verseElement.children[i].outerHTML = HTML;
  }
  markTitle();

  localStorage.gkSongChanges=JSON.stringify(songChanges);

  // findAllBV();
}
