var searchInput;
var searchOutput;
var searchList=[];

try{ console.log("embeddedTitleSearch:",embeddedTitleSearch); }
catch(e){ var embeddedTitleSearch=false; }

if(!embeddedTitleSearch){
  window.onload=function(){
    searchInput=document.getElementById('input');
    searchOutput=document.getElementById('output');
    loadSearchData();
  };
}

function initList(){
  var HTML="";
  for(var i=0; i<searchList.length; i++){
    HTML+=`<div onclick="slc('${searchList[i].uid}')">${searchList[i].titleRoma} • <small>[${searchList[i].uid}]</small></div>\n`;
  }
  if(!embeddedTitleSearch){
    searchOutput.innerHTML=HTML;
    search("");
  }
}

// var customResult=[];
// function doCustomSearch(){
//   for(var i=0; i<customSearch.length; i++){
//     search(customSearch[i]);
//   }
// }

// var prevS=""

if(!embeddedTitleSearch) selectsong=function(){};
function slc(i){
  for(j in listData){
    if(listData[j].uid==i) selectsong(j);
  }
  search("");
};

function search(s){
  if(s=="" && embeddedTitleSearch){
    searchOutput.style.display="none";
    fileviewer.style.display="block";
    songtitle.style.display="block";
    songauthor.style.display="block";
    songnotes.style.display="block";
    uidLetter.style.display="block";
    uidNumber.style.display="block";
    document.getElementById('authorLabel').style.display="block";
    document.getElementById('notesLabel').style.display="block";
    return;
  }
  if(embeddedTitleSearch){
    searchOutput.style.display="block";
    fileviewer.style.display="none";
    songtitle.style.display="none";
    songauthor.style.display="none";
    songnotes.style.display="none";
    uidLetter.style.display="none";
    uidNumber.style.display="none";
    document.getElementById('authorLabel').style.display="none";
    document.getElementById('notesLabel').style.display="none";
  }

  // originalS=s;
  s=s.toLowerCase().trim();
  S=s.toUpperCase();
  // s=transliterate(s, "ben");
  s=deDbl(deVowel(deNasal(deAspirate(similar(deDbl(unPunc(unDiac(s))))))));
  if(s.substr(-1)=='n') s=s.substr(0,s.length-1);
  // console.log(s);

  var uidSection=false;
  var titleSection=false;
  var authorSection=false;
  var contentSection=false;

  contentPreview=[];
  var HTML="";

  // EMPTY SEARCH
  if(s==""){
  for(var i=0; i<searchList.length; i++){
      HTML+=`<div class=slc onclick="slc('${searchList[i].uid}')">${searchList[i].titleRoma} • `
          +`<small>[${searchList[i].uid}]</small></div>\n`;
      continue;
    }
    searchOutput.innerHTML=HTML; return;
  }

  // UID SEARCH
  console.log(S);
  for(var i=0; i<searchList.length; i++){
    if(searchList[i].uid.search(S)!=-1){
      if(!uidSection) HTML += "<h3>UID Matches</h3>"; uidSection=true;
      HTML+=`<div class=slc onclick="slc('${searchList[i].uid}')">${searchList[i].titleRoma} • `
          +`<small><b>[${searchList[i].uid}]</b></small></div>\n`;
      // continue;
    }
  }

  // TITLE SEARCH
  if(s[0]=='j') s=s.substr(1);
  for(var i=0; i<searchList.length; i++){
    if(searchList[i].titleFuzz.search(s)!=-1){
      // customResult.push(`${searchList[i].uid}\t${originalS}`);
      // console.log(`${searchList[i].uid}\t${originalS}`);
      // if(prevS==originalS) console.log("!!!!");
      // prevS=originalS;
      if(!titleSection) HTML += "<h3>Title Matches</h3>"; titleSection=true;
      HTML+=`<div class=slc onclick="slc('${searchList[i].uid}')"><b>${searchList[i].titleRoma}</b> • `
          +`<small>[${searchList[i].uid}]</small></div>\n`;
      // continue;
    }
  }

  // AUTHOR SEARCH
  s=decodeAuthor(s);
  for(var i=0; i<searchList.length; i++){
    p=searchList[i].authorFuzz.search(s);
    if(p!=-1){
      if(!authorSection) HTML += "<h3>Author Matches</h3>"; authorSection=true;
      HTML+=`<div class=slc onclick="slc('${searchList[i].uid}')">${searchList[i].titleRoma} • `
          +`<small>[${searchList[i].uid}] ~ <b>${searchList[i].authorRoma}</b></small></div>\n`;
      // continue;
    }
  }

  // CONTENT SEARCH
  for(var i=0; i<searchList.length; i++){
    p=searchList[i].content.search(s);
    if(p!=-1){
      if(!contentSection) HTML += "<h3>Content Matches</h3>"; contentSection=true;
      HTML+=`<div class=slc onclick="slc('${searchList[i].uid}')">${searchList[i].titleRoma} • `
          +`<small>[${searchList[i].uid}] ~ <b id=${searchList[i].uid}>content loading...</b></small></div>\n`;
      if(contentPreview.length<108) contentPreview.push({p:p,i:i}); // match position in content, and index
      // continue;
    }
  }
  // if(embeddedTitleSearch) HTML=HTML.replace(/\n/g,'<br>');
  searchOutput.innerHTML=HTML;
}

var contentPreview=[];
var songs={};

setInterval(function(){
  if(contentPreview.length==0) return;
  // console.log(searchList,"!");
  pre = contentPreview.shift();
  loadSongForSearchPreview(searchList[pre.i].uid,pre);
}, 10);

function updateContentPreview(pre){
  // console.log(pre,searchList[pre.i].content);
  // console.log(songs[searchList[pre.i].uid].verses);

  p = pre.p;
  c = searchList[pre.i].content;

  c = c.substr(p);
  p = c.search('{'); c = c.substr(p+1); p = c.search('}');
  c = c.substr(0,p).split('|');

  line = songs[searchList[pre.i].uid].verses[c[0]].lines[c[1]];

  var content = "";
  for(var w=0; w<line.length; w++){
    content += line[w].w+line[w].h;
  }
  contentRoma = transliterate(content, songs[searchList[pre.i].uid].language);

  var el = document.getElementById(searchList[pre.i].uid);
  if(el) if(el.innerHTML!=contentRoma) el.innerHTML = contentRoma;
}

function loadSearchData(){
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
     var data = this.responseText;
     searchList = JSON.parse(data);
     initList();
    }
  };
  var fn = '../json/search.json';
  xhttp.open("GET", fn, true);
  xhttp.send();
}

function loadSongForSearchPreview(uid, params){
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
     var data = this.responseText;
     songs[uid] = JSON.parse(data);
     updateContentPreview(params);
    }
  };
  xhttp.open("GET", `../json/${uid}.json`, true);
  xhttp.send();
}

function decodeAuthor(s){
  s_ = s;
  if(s[0]=='s') s_ = s.substr(1);
  s_ = s_
    .replace("bt", "শ্রীল ভক্তিবিনোদ ঠাকুর")
    .replace("ndt", "শ্রীল নরোত্তম দাস ঠাকুর")
    .replace("ldt", "শ্রীল লোচন দাস ঠাকুর")
    .replace("vct", "শ্রীল বিশ্বনাথ চক্রবর্তী ঠাকুর")
    .replace("vdt", "শ্রীল বৃন্দাবন দাস ঠাকুর")
    .replace("kkg", "শ্রীল কৃষ্ণদাস কবিরাজ গোস্বামী")
    .replace("bsst", "শ্রীল ভক্তিসিদ্ধান্ত সরস্বতী গোস্বামী প্রভুপাদ")
    .replace("rg", "শ্রীল রূপ গোস্বামী")
    .replace("rdg", "শ্রীল রঘুনাথ দাস গোস্বামী")
    .replace("pdt", "শ্রীল প্রেমানন্দ দাস ঠাকুর");
  if(s_==s_) return s;
  return s;
  // if(s_.length>4) return s_0;
}

function unDiac(s){
  return s
    .replace(/ã|ā̃|ā/g, 'a')
    .replace(/ĩ|ī̃|ī/g, 'i')
    .replace(/ũ|ū̃|ū/g, 'u')

    .replace(/ẽ/g, 'e')
    .replace(/õ/g, 'o')

    .replace(/ṛ|ṝ/g, 'r')
    .replace(/ḷ|ḹ/g, 'l')

    .replace(/·/g, '')
    .replace(/ñ/g, 'n')
    .replace(/ṁ/g, 'm')
    .replace(/ḥ/g, 'h')
    .replace(/ṅ/g, 'n')
    .replace(/ṭ/g, 't')
    .replace(/ḍ/g, 'd')
    .replace(/ṇ/g, 'n')
    .replace(/ɽ/g, 'd')
    .replace(/ẏ/g, 'y')
    .replace(/ś/g, 's')
    .replace(/ṣ/g, 's');
}

function unNum(s){
  return s.replace(/[0-9]/g,'');
}

function unPunc(s){
  return s.replace(/\s|\-|\!|,|\.|\(|\)|’|~|\?|‘/g,'');
}

function deDbl(s){
  return s.replace(/(.)\1/g, '$1');
  // thissis a doubble bubble wordd yes.
}

function similar(s){
  return s
    .replace(/w/g, 'b')
    .replace(/v/g, 'b')
    .replace(/r/g, 'd')
    .replace(/y/g, 'j')
    .replace(/o/g, 'a')

    .replace(/f/g, 'p')
    .replace(/q/g, 'k')
    .replace(/x/g, 'k')
    .replace(/z/g, 'j');
}

function deAspirate(s){
  return s.replace(/(s|c|k|g|j|d|t|c|b|p)h/g, '$1');
  // kha gha jha dha tha cha bha pha
}

function deNasal(s){
  return s.replace(/n(k|g|g|c|j|t|d)/g, '$1');
  // kandiya kandiya ==> kadiya kadiya
}

function deVowel(s){
  return s.replace(/a|e|i|o|u/g, '');
}


// var customSearch=[];
