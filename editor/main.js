// elements
var songlist;
var songtitle;
var songauthor;
var songnotes;
var uidLetter, uidNumber;
var fileviewer;

var dataFolder = "json/";

// song-data in memory
var songs=[];
var titleChanges=[];
var authorChanges=[];
var notesChanges=[];
var songChanges={};

// ordering and list data
var listData;
var listOrder=[];
var fallBackSong=0;
var currentSong=0;

//sort these:::
var tabNames=['bang','over', 'roma', 'syno', 'tran'];
var listOrderChanges=[];
var mode={bang:true,over:false,syno:false,tran:false};
var legend={w:"word",h:"hint",s:"synonym",o:"override"};

var updateViewerOffset = 0;
var max=1008;
var lastV = [];
var count=0;

var language="eng";

// ========================================================================== //

function resizeAnnoyingContainers(){
  document.getElementById('SLcontainer').style.marginTop=`${45+16-40}px`;
  document.getElementById('SLcontainer').style.height=`${window.innerHeight-44-45-41}px`;
  document.getElementById('FVcontainer').style.height=`${window.innerHeight-88}px`;
  document.getElementById('FVcontainer').style.width=`${window.innerWidth-400}px`;
  li=document.getElementsByClassName('list-item')[0].offsetWidth;
  document.getElementById('gkSearch').style.width=`${li-50}px`;
  document.getElementById('addbutton').style.width=`${li-22}px`;
}

// ========================================================================== //

var CSSretryCount=0;
function reloadCSS() {
  try{
    if(document.styleSheets[0].rules.length > 0){
      console.log("CSS Loaded.");
      resizeAnnoyingContainers();
      return;
    }
  } catch(e){}
  if(CSSretryCount>10) return;
  CSSretryCount++;

  // remove any existing styleSheet fails
  var e = document.querySelectorAll('link[rel=stylesheet]');
  for(var i=0;i<e.length;i++){ e[i].parentNode.removeChild(e[i]); }

  // reload styleSheet from source
  var link = document.createElement("link");
  link.rel = "stylesheet";
  link.href = "main.css";
  document.getElementsByTagName("head")[0].appendChild(link);
  setTimeout(reloadCSS,10);
}

// ========================================================================== //

var embeddedTitleSearch=true;
window.onresize=resizeAnnoyingContainers;
window.onload=function(){
  songlist = document.getElementById("songlist");
  songtitle = document.getElementById("songtitle");
  songauthor = document.getElementById("songauthor");
  songnotes = document.getElementById("songnotes");
  uidLetter = document.getElementById("uidLetter");
  uidNumber = document.getElementById("uidNumber");
  fileviewer = document.getElementById("fileviewer");
  searchInput=document.getElementById('gkSearch');
  searchOutput=document.getElementById('searchResults');

  reloadCSS();
  loadList();
  loadSearchData();
};

// ========================================================================== //

document.addEventListener("paste", function(e) {
  e.preventDefault();
  var text = (e.originalEvent || e).clipboardData.getData('text/plain');
  document.execCommand("insertHTML", false, text);
});

/*
document.onkeyup=function(e){
  if(e.key=='Dead' && e.code=='KeyF'){
    var sel = window.getSelection();
    var offset = sel.focusOffset;
    setTimeout(fixDoubleHalant, 0, {el:e.target,o:offset});
  }
}
*/

function fixDoubleHalant(params){
  var el=params.el;
  var offset=params.o-1;
  var text = el.innerText;
  el.innerText = text.substr(0,offset)
               + text.substr(offset+1);

  var sel = window.getSelection();
  sel.collapse(el.firstChild, offset);

  // var sel = window.getSelection();
  // var range = sel.getRangeAt(0);
  // var preCaretRange = range.cloneRange();
  //     preCaretRange.selectNodeContents(el);
  //     preCaretRange.setEnd(range.endContainer, range.endOffset);
  // var caretOffset = preCaretRange.toString().length-1;

  // el.innerHTML = text.substr(0,text.length);
  //
  // rng.setStart(el, caretOffset);
  // rng.collapse(true);
  // sel.removeAllRanges();
  // // sel.addRange(range);
  // el.focus();
}

// ========================================================================== //

function saveData(data){
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
     // console.log( this.responseText );
     alert("Success :)");
     location.href=location.href;
    }
  };
  xhttp.open("POST", `save`, true);
  xhttp.send(data);
}

// ========================================================================== //

function loadList(){
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var data = this.responseText;
      listData = JSON.parse(data);
      for(var i=0; i<listData.length; i++){
        listOrder.push(`${i}`);
        listData[i].new_uid = listData[i].uid;
        for(var p=0; p<listData[i].uid.length; p++)
          if( !isNaN(listData[i].uid[p]) ) break;
        listData[i].letter = listData[i].uid.substr(0,p);
        listData[i].number = listData[i].uid.substr(p);
      }
      reload();
      generatelist();

      document.getElementById('SLcontainer').scroll(0,
        songlist.getElementsByClassName("list-item active")
        [0].offsetTop-window.innerHeight/2);

    }
  };
  xhttp.open("GET", `${dataFolder}_list.json`, true);
  xhttp.send();
}

// ========================================================================== //



// ========================================================================== //

function loadSong(fn, index, callback){
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
     var data = this.responseText;
     songs[index] = JSON.parse(data);
     // updateViewer(index);
     if(callback) callback()
     else setTimeout(updateViewer,0,index);
    }
  };
  xhttp.open("GET", fn, true);
  xhttp.send();
}

// ========================================================================== //

function generatelist(){
  var outputHTML = '';
  var letterHeading = '';
  var title;

  for(i in listOrder){
    var j = listOrder[i];

    // if( listData[j].uid.substr(0,19)=="MARKED-FOR-DELETION" ) continue;

    headingMatch = (listData[j].letter==letterHeading);
    if(!headingMatch){
      letterHeading = listData[j].letter;
      letterIndex=listData[j].uid=="A0"?0:1;
    }

    generated_uid = letterHeading+(letterIndex++);
    listData[j].new_uid = generated_uid;

    edited=false;
    if(titleChanges[j]) edited=true;
    titleText = (edited?titleChanges[j]:listData[j].title);
    titleText = mode.roma?transliterate(titleText, listData[j].language):titleText;
    titleText = titleText.replace(/\[comma\]/g,",");
    title = `
      ${edited?'<span class=edited>':''}
      ${titleText}
      ${edited?'</span>':''}
      <small>[${(listData[j].uid!=listData[j].new_uid ?
        `<s>${listData[j].uid}</s>&rarr;`
        :'')}${generated_uid}]</small>${scanSongChanges(j)?' <s>*</s>':''}`;

    outputHTML +=
      `${(!headingMatch ?
          `<div class=letterHeading>${listData[j].uid=="A0"?"HELP":letterHeading}</div>`
          :'')}
      <div
        class="list-item ${j==currentSong?'active':''}"
        draggable=true
        onclick="selectsong(${j})"
        index=${j}
      >${title}</div>`;
  }
  songlist.innerHTML = outputHTML;
  bvIndex=-1;
  showsong(currentSong);
}

// ========================================================================== //

function selectsong(n){
  updateViewerOffset = 0;
  lastV = [];
  fileviewer.innerHTML = "Rendering Atoms...";

  var j = listOrder.indexOf(`${currentSong}`);
  var k = listOrder.indexOf(`${n}`);

  fallBackSong = currentSong;
  currentSong = n;
  localStorage.gkCurrentSong = n;
  // generatelist();
  currentSongList = songlist.getElementsByClassName("list-item");
  currentSongList[j].setAttribute('class', 'list-item');
  currentSongList[k].setAttribute('class', 'list-item active');
  // showsong(currentSong);
  generatelist();
  // setTimeout(generatelist,0);
}

// ========================================================================== //

function showsong(n){
  if(songs[n]) setTimeout(updateViewer,0,n);
  else loadSong( `${dataFolder}${listData[n].uid}.json`, n);
}

// ========================================================================== //

function reload(){

  if(localStorage.gkTitleChanges)
    if(localStorage.gkTitleChanges.split(',').join('')=="")
      delete localStorage.gkTitleChanges;
  if(localStorage.gkAuthorChanges)
    if(localStorage.gkAuthorChanges.split(',').join('')=="")
      delete localStorage.gkAuthorChanges;
  if(localStorage.gkNotesChanges)
    if(localStorage.gkNotesChanges.split(',').join('')=="")
      delete localStorage.gkNotesChanges;

  listOrder_=false;
  if(localStorage.gkModes) mode = JSON.parse(localStorage.gkModes);
  if(localStorage.gkListOrder) listOrder_ = localStorage.gkListOrder.split(',');
  if(localStorage.gkCurrentSong) currentSong = localStorage.gkCurrentSong;
  if(localStorage.gkTitleChanges) titleChanges = localStorage.gkTitleChanges.split(',');
  if(localStorage.gkAuthorChanges) authorChanges = localStorage.gkAuthorChanges.split(',');
  if(localStorage.gkNotesChanges) notesChanges = localStorage.gkNotesChanges.split(',');
  if(localStorage.gkSongChanges) songChanges = JSON.parse(localStorage.gkSongChanges);

  if(listOrder_) for(var i=0; i<listOrder_.length; i++) listOrder[i]=listOrder_[i];

  tabs=document.getElementsByClassName('tab');
  for(var i=0; i<tabs.length; i++)
    tabs[i].setAttribute('class', `tab button${mode[tabNames[i]]?' active':''}`);

  if(mode.roma){
    songtitle.setAttribute('class','headerField frozen');
    songauthor.setAttribute('class','headerField frozen');
    songnotes.setAttribute('class','headerField frozen');
    songtitle.setAttribute('contenteditable', false);
    songauthor.setAttribute('contenteditable', false);
    songnotes.setAttribute('contenteditable', false);
  }
}
