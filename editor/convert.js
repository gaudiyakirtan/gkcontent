//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// BEN-BEN TRANSLITERATION SECTION --
// transliterate bengali song written in bangla
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

var bb_ind_vwl = ['অ','আ','ই','ঈ','উ','ঊ','ঋ','ৠ','ঌ','ৡ','এ','ঐ','ও','ঔ'];
var bb_depend_vwl = ['া','ি','ী','ু','ূ','ৃ','ৄ','ৢ','ৣ','ে','ৈ','ো','ৌ'];
var bb_con = ['ক','খ','গ','ঘ','ঙ','চ','ছ','জ','ঝ','ঞ','ট','ঠ','ড','ঢ','ণ','ড়','ঢ়','ত','থ','দ','ধ','ন','প','ফ','ব','ভ','ম','য','য়','র','ল','ৱ','শ','ষ','স','হ'];

bb_ind_vwl = bb_ind_vwl.join('|');
bb_depend_vwl = bb_depend_vwl.join('|');
bb_con = bb_con.join('|');

var bb_interpuncterfy = ['অ'].concat(bb_con).concat(['ঁ']).join('|');

var t_bb_ind_vwl_n = {'অঁ':'ã','আঁ':'ā̃','ইঁ':'ĩ','ঈঁ':'ī̃','উঁ':'ũ','ঊঁ':'ū̃','এঁ':'ẽ','ঐঁ':'aĩ','ওঁ':'õ','ঔঁ':'aũ'};
var t_bb_ind_vwl = {'অ':'a','আ':'ā','ই':'i','ঈ':'ī','উ':'u','ঊ':'ū','ঋ':'ṛ','ৠ':'ṝ','ঌ':'ḷ','ৡ':'ḹ','এ':'e','ঐ':'ai','ও':'o','ঔ':'au','ং':'ṁ','ঃ':'ḥ'};
var t_bb_dpd_vwl_n = {'াঁ':'ā̃','িঁ':'ĩ','ীঁ':'ī̃','ুঁ':'ũ','ূঁ':'ū̃','েঁ':'ẽ','ৈঁ':'aĩ','োঁ':'o','ৌঁ':'aũ'};
var t_bb_dpd_vwl = {'া':'ā','ি':'i','ী':'ī','ু':'u','ূ':'ū','ৃ':'ṛ','ৄ':'ṝ','ৢ':'ḷ','ৣ':'ḹ','ে':'e','ৈ':'ai','ো':'o','ৌ':'au'};
var t_bb_con = {'ক্':'k','কঁ':'kã','ক':'ka','খ্':'kh','খঁ':'khã','খ':'kha','গ্':'g','গঁ':'gã','গ':'ga','ঘ্':'gh','ঘঁ':'ghã','ঘ':'gha','ঙ্':'ṅ','ঙ':'ṅa',
             'চ্':'c','চঁ':'cã','চ':'ca','ছ্':'ch','ছঁ':'chã','ছ':'cha','জ্':'j','জঁ':'jã','জ':'ja','ঝ্':'jh','ঝঁ':'jhã','ঝ':'jha','ঞ্':'ñ','ঞ':'ña',
             'ত্':'t','তঁ':'tã','ত':'ta','থ্':'th','থঁ':'thã','থ':'tha','দ্':'d','দঁ':'dã','দ':'da','ধ্':'dh','ধঁ':'dhã','ধ':'dha','ন্':'n','নঁ':'nã','ন':'na','ৎ':'t',
             'ট্':'ṭ','টঁ':'ṭã','ট':'ṭa','ঠ্':'ṭh','ঠঁ':'ṭhã','ঠ':'ṭha','ড্':'ḍ','ডঁ':'ḍã','ড':'ḍa','ঢ্':'ḍh','ঢঁ':'ḍhã','ঢ':'ḍha','ণ্':'ṇ','ণ':'ṇa','ড়্':'ḓ','ড়ঁ':'ḓã','ড়':'ḓa','ঢ়্':'ḓh','ঢ়ঁ':'ḓhã','ঢ়':'ḓha',
             'প্':'p','পঁ':'pã','প':'pa','ফ্':'ph','ফঁ':'phã','ফ':'pha','ব্':'b','বঁ':'bã','ব':'ba','ভ্':'bh','ভঁ':'bhã','ভ':'bha','ম্':'m','মঁ':'mã','ম':'ma',
             'য্':'y','যঁ':'yã','য':'ya','য়্':'ẏ','য়ঁ':'ẏã','য়':'ẏa','র্':'r','রঁ':'rã','র':'ra','ল্':'l','লঁ':'l ̌','ল':'la','ৱ্':'v','ৱঁ':'vã','ৱ':'va',
             'শ্':'ś','শঁ':'śã','শ':'śa','ষ্':'ṣ','ষঁ':'ṣã','ষ':'ṣa','স্':'s','সঁ':'sã','স':'sa','হ্':'h','হঁ':'hã','হ':'ha'};
var t_bb_num = {'০':'0','১':'1','২':'2','৩':'3','৪':'4','৫':'5','৬':'6','৭':'7','৮':'8','৯':'9'};

function transBenBen(str){
  if(!str) return "";

  str = str.replace(/ড়/g, 'ড়');
  str = str.replace(/ঢ়/g, 'ঢ়');
  str = str.replace(/য়/g, 'য়');
  str = str.replace(/ব়/g, 'র');
  str = str.replace(/ো/g, 'ো');
  str = str.replace(/ৌ/g, 'ৌ');

  str = str.replace(/ঞি/g, 'ইঁ');
  str = str.replace(/ঞী/g, 'ইঁ');
  // str = str.replace(/(?<!্)(ঞা)/, 'য়া'); // negative look behind not supported. see sri example?

  str = str.replace(/ড়/g, 'ড়');
  str = str.replace(/ঢ়/g, 'ঢ়');
  str = str.replace(/য়/g, 'য়');
  str = str.replace(/ব়/g, 'র');

  var regexA = new RegExp(`(${bb_interpuncterfy})(ই|উ)`,      "g");
  var regexB = new RegExp(`(${bb_con})(${bb_depend_vwl})`, "g");
  var regexC = new RegExp('([kgṅcjñṭḍṇtdnpbmyrlśṣsh])(y)', "g");

  str = str.replace(regexA, function(m,g1,g2){ return `${g1}·${g2}`; });
  for(var key in t_bb_ind_vwl_n) str = str.replace(new RegExp(key,"g"), t_bb_ind_vwl_n[key]);
  for(var key in t_bb_ind_vwl)   str = str.replace(new RegExp(key,"g"), t_bb_ind_vwl[key]);

  str = str.replace(regexB, function(m,g1,g2){ return `${g1}্${g2}`; });
  for(var key in t_bb_dpd_vwl_n) str = str.replace(new RegExp(key,"g"), t_bb_dpd_vwl_n[key]);
  for(var key in t_bb_dpd_vwl)   str = str.replace(new RegExp(key,"g"), t_bb_dpd_vwl[key]);
  for(var key in t_bb_con)       str = str.replace(new RegExp(key,"g"), t_bb_con[key]);

  str = str.replace(regexC, function(m,g1){ return `${g1}ẏ`; });
  str = str.replace('ঽ','’');

  for(var key in t_bb_num)       str = str.replace(new RegExp(key,"g"), t_bb_num[key]);

  // if(exact_exceptions.indexOf(str)!=-1) return str;

  // str = str.replace(/śrī(?!\s)/g, "śrī ");
  // str = str.replace(/r(t|m|b|v|d|k){2}/g, "r$1"); // *rtt*  *rmm* *rbb* *rvv* *rkk*
  // str = str.replace(/rẏy/g, "rẏ");
  // str = str.replace(/(nd|k|g|ṅ|c|j|ñ|t|d|n|ṭ|ḍ|ṇ|ḓ|p|b|m|y|ẏ|r|l|ś|ṣ|s|h)b/g, "$1v");

  // for(var i=0; i<contained_exceptions.length; i++){
  //   var p=str.search(contained_exceptions[i]);
  //   if(p!=-1) return str;
  // }

  return str;
  // return str.replace(/bh/g, '%%%')
  //   .replace(/b/g, 'v')
  //   .replace(/%%%/g, 'bh');
}




//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// SAN-BEN TRANSLITERATION SECTION --
// transliterate sanskrit song written in bangla --> TO DEVANAGARI
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

t_sb_sd = {'অ':'अ','আ':'आ','ই':'इ','ঈ':'ई','উ':'उ','ঊ':'ऊ','ঋ':'ऋ','ৠ':'ॠ','ঌ':'ऌ','ৡ':'ॡ',
           'এ':'ए','ঐ':'ऐ','ও':'ओ','ঔ':'औ',
           'া':'ा','ি':'ि','ী':'ी','ু':'ु','ূ':'ू','ৃ':'ृ','ৄ':'ॄ','ৢ':'ॢ','ৣ':'ॣ',
           'ে':'े','ৈ':'ै','ো':'ो','ৌ':'ौ',
           'ং':'ं','ঃ':'ः','ঁ':'ँ','ঽ':'ऽ','্':'्',
           'ক':'क','খ':'ख','গ':'ग','ঘ':'घ','ঙ':'ङ',
           'চ':'च','ছ':'छ','জ':'ज','ঝ':'झ','ঞ':'ञ',
           'ত':'त','থ':'थ','দ':'द','ধ':'ध','ন':'न','ৎ':'त्',
           'ট':'ट','ঠ':'ठ','ড':'ड','ঢ':'ढ','ণ':'ण','ড়':'ड़','ড়':'ड़','ঢ়':'ढ़',
           'প':'प','ফ':'फ','ব':'ब','ভ':'भ','ম':'म',
           'য':'य','য়':'य','র':'र','ল':'ल','ৱ':'व',
           'শ':'श','ষ':'ष','স':'स','হ':'ह',
           '০':'०','১':'१','২':'२','৩':'३','৪':'४','৫':'५','৬':'६','৭':'७','৮':'८','৯':'९'};

function transSanBen(str){
  if(!str) return "";

  str = str.replace(/ঞি/g, 'ইঁ');
  str = str.replace(/ঞী/g, 'ইঁ');
  str = str.replace(/ঞা/g, 'য়া');

  str = str.replace(/ড়/g, 'ড়');
  str = str.replace(/ঢ়/g, 'ঢ়');
  str = str.replace(/য়/g, 'য়');
  str = str.replace(/ব়/g, 'র');
  // str = str.replace(/ৱ্ৱ/g, 'ৱ');

  // str = str.replace(new RegExp('(ে)(.)(া)', "g"), function(m,g1,g2){ return `${g2}ো`; });
  // str = str.replace(new RegExp('(ে)(.)(ৗ)', "g"), function(m,g1,g2){ return `${g2}ৌ`; });

  var ch, out="";
  for(var c=0; c<str.length; c++){
    ch = t_sb_sd[str[c]];
    if(!ch) out+=str[c];
    else out+=ch;
  }
  // for(var key in t_sb_sd) str = str.replace(new RegExp(key,"g"), t_sb_sd[key]);

  return out;
}




//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// SAN-DEV TRANSLITERATION SECTION --
// transliterate sanskrit song written in devanagari --> TO ENGLISH SCRIPT
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

var sd_ind_vwl = ['अ','आ','इ','ई','उ','ऊ','ऋ','ॠ','ऌ','ॡ','ए','ऐ','ओ','औ'];
var sd_depend_vwl = ['ा','ि','ी','ु','ू','ृ','ॄ','ॢ','ॣ','े','ै','ो','ौ'];
var sd_con = ['क','ख','ग','ঘ','ङ','च','छ','ज','झ','ञ','ट','ठ','ड','ढ','ण','ड़','ढ़','त','थ','द','ध','न','प','फ','ब','भ','म','य','य़','र','ल','व','श','ष','स','ह'];

sd_ind_vwl = sd_ind_vwl.join('|');
sd_depend_vwl = sd_depend_vwl.join('|');
sd_con = sd_con.join('|');

var sd_interpuncterfy = [].concat(sd_ind_vwl).concat(sd_depend_vwl).concat(sd_con).concat(['ঁ']).join('|');

var t_sd_ind_vwl_n = {'अँ':'ã','आँ':'ā̃','इँ':'ĩ','ईँ':'ī̃','उँ':'ũ','ऊँ':'ū̃','एँ':'ẽ','ऐँ':'aĩ','ओँ':'o','ऒँ':'aũ'};
var t_sd_ind_vwl = {'अ':'a','आ':'ā','इ':'i','ई':'ī','उ':'u','ऊ':'ū','ऋ':'ṛ','ॠ':'ṝ','ऌ':'ḷ','ॡ':'ḹ','ए':'e','ऐ':'ai','ओ':'o','औ':'au','ं':'ṁ','ः':'ḥ'};
var t_sd_dpd_vwl_n = {'ाँ':'ā̃','िँ':'ĩ','ीँ':'ī̃','ुँ':'ũ','ूँ':'ū̃','ेँ':'ẽ','ैँ':'aĩ','ोँ':'o','ौँ':'aũ'};
var t_sd_dpd_vwl = {'ा':'ā','ि':'i','ी':'ī','ु':'u','ू':'ū','ृ':'ṛ','ॄ':'ṝ','ॢ':'ḷ','ॣ':'ḹ','े':'e','ै':'ai','ो':'o','ौ':'au'};
var t_sd_con = {'क्':'k','कँ':'kã','क':'ka','ख्':'kh','खँ':'khã','ख':'kha','ग्':'g','गँ':'gã','ग':'ga','घ्':'gh','घँ':'ghã','घ':'gha','ङ्':'ṅ','ङ':'ṅa',
                'च्':'c','चँ':'cã','च':'ca','छ्':'ch','छँ':'chã','छ':'cha','ज्':'j','जँ':'jã','ज':'ja','झ्':'jh','झँ':'jhã','झ':'jha','ञ्':'ñ','ञ':'ña',
                'त्':'t','तँ':'tã','त':'ta','थ्':'th','थ':'thã','थ':'tha','द्':'d','दँ':'dã','द':'da','ध्':'dh','धँ':'dhã','ध':'dha','न्':'n','नँ':'nã','न':'na',
                'ट्':'ṭ','टँ':'ṭã','ट':'ṭa','ठ्':'ṭh','ठँ':'ṭhã','ठ':'ṭha','ड़्':'ḍ','ड्':'ḍ','डँ':'ḍã','ड':'ḍa','ढ्':'ḍh','ढँ':'ḍhã','ढ':'ḍha','ण्':'ṇ','ण':'ṇa',
                'प्':'p','पँ':'pã','प':'pa','फ्':'ph','फँ':'phã','फ':'pha','ब्':'b','बँ':'bã','ब':'ba','भ्':'bh','भँ':'bhã','भ':'bha','म्':'m','मँ':'mã','म':'ma',
                'य्':'y','यँ':'yã','य':'ya','य़्':'ẏ','य़ँ':'ẏã','य़':'ẏa','र्':'r','रँ':'rã','र':'ra','ल्':'l','लँ':'lã','ल':'la','व्':'v','वँ':'vã','व':'va',
                'श्':'ś','शँ':'śã','श':'śa','ष्':'ṣ','षँ':'ṣã','ष':'ṣa','स्':'s','सँ':'sã','स':'sa','ह्':'h','हँ':'hã','ह':'ha'};
var t_sd_num = {'०':'0','१':'1','२':'2','३':'3','४':'4','५':'5','६':'6','७':'7','८':'8','९':'9'};

function transSanDev(str){
  if(!str) return "";

  str = str.replace(/'ड़'/g, 'ड़');
  str = str.replace(/'ढ़'/g, 'ढ़');
  str = str.replace(/'य़'/g, 'य़');
  str = str.replace(/'ाे'/g, 'ो');
  str = str.replace(/'ाै'/g, 'ौ');

  for(var key in t_sd_ind_vwl_n) str = str.replace(new RegExp(key,"g"), t_sd_ind_vwl_n[key]);
  for(var key in t_sd_ind_vwl) str = str.replace(new RegExp(key,"g"), t_sd_ind_vwl[key]);

  var regexA = new RegExp(`(${sd_con})(${sd_depend_vwl})`, "g");
  str = str.replace(regexA, function(m,g1,g2){ return `${g1}्${g2}`; });

  for(var key in t_sd_dpd_vwl_n) str = str.replace(new RegExp(key,"g"), t_sd_dpd_vwl_n[key]);
  for(var key in t_sd_dpd_vwl) str = str.replace(new RegExp(key,"g"), t_sd_dpd_vwl[key]);
  for(var key in t_sd_con) str = str.replace(new RegExp(key,"g"), t_sd_con[key]);

  str = str.replace('ऽ', '’');
  for(var key in t_sd_num) str = str.replace(new RegExp(key,"g"), t_sd_num[key]);
  str = str.replace(/ॐ/g, 'oṁ');
  str = str.replace(/ঢ়/g, 'ḍa');

  return str;
}





//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// HIN-DEV TRANSLITERATION SECTION --
// transliterate hindi song written in devanagari
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

var hd_ind_vwl = ['अ','आ','इ','ई','उ','ऊ','ऋ','ॠ','ऌ','ॡ','ए','ऐ','ओ','औ'];
var hd_depend_vwl = ['ा','ि','ी','ु','ू','ृ','ॄ','ॢ','ॣ','े','ै','ो','ौ'];
var hd_con = ['क','ख','ग','ঘ','ङ','च','छ','ज','झ','ञ','ट','ठ','ड','ढ','ण','ड़','ढ़','त','थ','द','ध','न','प','फ','ब','भ','म','य','य़','र','ल','व','श','ष','स','ह'];

hd_ind_vwl = hd_ind_vwl.join('|');
hd_depend_vwl = hd_depend_vwl.join('|');
hd_con = hd_con.join('|');

var hd_interpuncterfy = ['अ'].concat(hd_con).concat(['|ँ']).join('|');

var t_hd_ind_vwl_n = {'अँ':'ã','आँ':'ā̃','इँ':'ĩ','ईँ':'ī̃','उँ':'ũ','ऊँ':'ū̃','एँ':'ẽ','ऐँ':'aĩ','ओँ':'õ','ऒँ':'aũ'};
var t_hd_ind_vwl = {'अ':'a','आ':'ā','इ':'i','ई':'ī','उ':'u','ऊ':'ū','ऋ':'ṛ','ॠ':'ṝ','ऌ':'ḷ','ॡ':'ḹ','ए':'e','ऐ':'ai','ओ':'o','औ':'au','ं':'ṁ','ः':'ḥ'};
var t_hd_dpd_vwl_n = {'ाँ':'ā̃','िँ':'ĩ','ीँ':'ī̃','ुँ':'ũ','ूँ':'ū̃','ेँ':'ẽ','ैँ':'aĩ','ोँ':'o','ौँ':'aũ'};
var t_hd_dpd_vwl = {'ा':'ā','ि':'i','ी':'ī','ु':'u','ू':'ū','ृ':'ṛ','ॄ':'ṝ','ॢ':'ḷ','ॣ':'ḹ','े':'e','ै':'ai','ो':'o','ौ':'au'};

var t_hd_con = {'क्':'k','कँ':'kã','क':'ka','ख्':'kh','खँ':'khã','ख':'kha','ग्':'g','गँ':'gã','ग':'ga','घ्':'gh','घँ':'ghã','घ':'gha','ङ्':'ṅ','ङ':'ṅa',
             'च्':'c','चँ':'cã','च':'ca','छ्':'ch','छँ':'chã','छ':'cha','ज्':'j','जँ':'jã','ज':'ja','झ्':'jh','झँ':'jhã','झ':'jha','ञ्':'ñ','ञ':'ña',
             'त्':'t','तँ':'tã','त':'ta','थ्':'th','थ':'thã','थ':'tha','द्':'d','दँ':'dã','द':'da','ध्':'dh','धँ':'dhã','ध':'dha','न्':'n','नँ':'nã','न':'na',  // changed all of these: "ɽ" ==> "ḓ"
             'ट्':'ṭ','टँ':'ṭã','ट':'ṭa','ठ्':'ṭh','ठँ':'ṭhã','ठ':'ṭha','ड्':'ḍ','डँ':'ḍã','ड':'ḍa','ढ्':'ḍh','ढँ':'ḍhã','ढ':'ḍha','ण्':'ṇ','ण':'ṇa','ड़्':'ḓ','ड़ँ':'ḓã','ड़':'ḓa','ढ़्':'ḓh','ढ़ँ':'ḓhã','ढ़':'ḓha', // 'ड़्':'ḓ','ड़ँ':'ḓã','ड़':'ḓa','ढ़्':'ḓh','ढ़ँ':'ḓhã','ढ़':'ḓha', stupedly coded form using nukta <-- what a noob[ta] ;P
             'प्':'p','पँ':'pã','प':'pa','फ्':'ph','फँ':'phã','फ':'pha','ब्':'b','बँ':'bã','ब':'ba','भ्':'bh','भँ':'bhã','भ':'bha','म्':'m','मँ':'mã','म':'ma',
             'य्':'y','यँ':'yã','य':'ya','य़्':'ẏ','य़ँ':'ẏã','य़':'ẏa','र्':'r','रँ':'rã','र':'ra','ल्':'l','लँ':'lã','ल':'la','व्':'v','वँ':'vã','व':'va',
             'श्':'ś','शँ':'śã','श':'śa','ष्':'ṣ','षँ':'ṣã','ष':'ṣa','स्':'s','सँ':'sã','स':'sa','ह्':'h','हँ':'hã','ह':'ha'};
var t_hd_num = {'०':'0','१':'1','२':'2','३':'3','४':'4','५':'5','६':'6','७':'7','८':'8','९':'9'};
var t_hd_nasal_vwl = {'aṁ':'ã','āṁ':'ā̃','iṁ':'ĩ','īṁ':'ī̃','uṁ':'ũ','ūṁ':'ū̃','eṁ':'ẽ','oṁ':'õ'};

function transHinDev(str){
  if(!str) return "";

  str = str.replace(/ड़/g, 'ड़');
  str = str.replace(/ढ़/g, 'ढ़');
  str = str.replace(/य़/g, 'य़');
  str = str.replace(/ाे/g, 'ो');
  str = str.replace(/ाै/g, 'ौ');

  var regexA = new RegExp(`(${hd_interpuncterfy})(इ|उ)`,        "g");
  var regexB = new RegExp(`(${hd_con})(${hd_depend_vwl})`,      "g");

  str = str.replace(regexA, function(m,g1,g2){ return `${g1}·${g2}`; });
  for(var key in t_hd_ind_vwl_n) str = str.replace(new RegExp(key,"g"), t_hd_ind_vwl_n[key]);
  for(var key in t_hd_ind_vwl)   str = str.replace(new RegExp(key,"g"), t_hd_ind_vwl[key]);

  str = str.replace(regexB, function(m,g1,g2){ return `${g1}्${g2}`; });
  for(var key in t_hd_dpd_vwl_n) str = str.replace(new RegExp(key,"g"), t_hd_dpd_vwl_n[key]);
  for(var key in t_hd_dpd_vwl)   str = str.replace(new RegExp(key,"g"), t_hd_dpd_vwl[key]);
  for(var key in t_hd_con)       str = str.replace(new RegExp(key,"g"), t_hd_con[key]);

  str = str.replace(new RegExp('(ṁ)(k|g)',"g"), function(m,g1,g2){ return `ṅ${g2}`; });
  str = str.replace(new RegExp('(ṁ)(c|j)',"g"), function(m,g1,g2){ return `ñ${g2}`; });
  str = str.replace(new RegExp('(ṁ)(ṭ|ḍ)',"g"), function(m,g1,g2){ return `ṇ${g2}`; });
  str = str.replace(new RegExp('(ṁ)(t|d)',"g"), function(m,g1,g2){ return `n${g2}`; });
  str = str.replace(new RegExp('(ṁ)(p|b)',"g"), function(m,g1,g2){ return `m${g2}`; });
  for(var key in t_hd_con) str = str.replace(new RegExp(key,"g"), t_hd_con[key]);
  for(var key in t_hd_nasal_vwl) str = str.replace(new RegExp(key,"g"), t_hd_nasal_vwl[key]);
  for(var key in t_hd_num)  str = str.replace(new RegExp(key,"g"), t_hd_num[key]);

  return str;
}





//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// COMMON SECTION
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function autodetect(s){
  for(var i=0; i<Math.min(10,s.length); i++)
    if(  bb_ind_vwl.indexOf(s[i]) > -1
      || bb_depend_vwl.indexOf(s[i]) >-1
      || bb_con.indexOf(s[i]) >-1 ) return "ben";

  for(var i=0; i<Math.min(10,s.length); i++)
    if(  hd_ind_vwl.indexOf(s[i]) > -1
      || hd_depend_vwl.indexOf(s[i]) >-1
      || hd_con.indexOf(s[i]) >-1 ) return "dev";

  // console.log( s, "unknown" );
  return "unknown";
}

function transliterate(str, lang){
  if(!str) return "";

  // autodetect devanagari / bangla
  var script = autodetect(str);
  if(script == "unknown") return str;
  // console.log(lang, script);

  var separators = " -!?,.()’~‘";
  var output="";
  var word="";
  var z=str.length-1;

  function haribol(){
    // output is Eng script:
    if(lang=="ben" && script=="ben") word=transBenBen(word); // transliterate bangla song wirtten in bangla script
    if(lang=="hin" && script=="dev") word=transHinDev(word); // transliterate hindi song wirtten in devanagari script

    // SanBen=>SanDev-->SanDev=>SanEng == SanBen=>SanEng:
    if(lang=="san" && script=="ben"){
      word=transSanBen(word); // transliterate sanskrit song wirtten in bangla script --> outputs devanagari script
      word=transSanDev(word); // transliterate sanskrit song wirtten in devanagari script --> outputs english script
    }
    // word = word.replace(/śrīla/g, "%%%");
    // word = word.replace(/śrī(?!\s)/g, "śrī ");
    // word = word.replace(/%%%/g, "śrīla");

    word = word.replace(/rtt/g, "rt"); // *rtt*  *rmm* *rbb* *rvv* *rdd* *rkk*
    word = word.replace(/rmm/g, "rm"); // *rtt*  *rmm* *rbb* *rvv* *rdd* *rkk*
    word = word.replace(/rbb/g, "rb"); // *rtt*  *rmm* *rbb* *rvv* *rdd* *rkk*
    word = word.replace(/rvv/g, "rv"); // *rtt*  *rmm* *rbb* *rvv* *rdd* *rkk*
    word = word.replace(/rdd/g, "rd"); // *rtt*  *rmm* *rbb* *rvv* *rdd* *rkk*
    word = word.replace(/rkk/g, "rk"); // *rtt*  *rmm* *rbb* *rvv* *rdd* *rkk*

    word = word.replace(/rẏy/g, "rẏ");

    word = word.replace(/bh/g, '%%%')
      .replace(/(nd|k|g|ṅ|c|j|ñ|t|d|n|ṭ|ḍ|ṇ|ḓ|p|b|y|ẏ|r|l|ś|ṣ|s|h)b/g, "$1v")
      .replace(/%%%/g, 'bh');

    if(lang=="san" && script=="dev"){
      // respond to hidden hyphen/space markers
    }
  }

  for(var c=0; c<=z; c++){
    word+=str[c];
    if(separators.indexOf(str[c])!=-1){
      var fc=str[c]; // finishing character (i.e. punctuation)
      word=word.substr(0,word.length-1);
      haribol();
      output+=word+fc;
      word="";
    }
    else if(c==z){
      // las word in a phrase ...no finishing character
      word=word.substr(0,word.length);
      haribol();
      output+=word;
      word="";
    }
  }

  return output;
}

try{ module.exports = { transliterate }; }catch(e){}
