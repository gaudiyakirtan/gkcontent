
function reconcileListOrderChain(a){
  origin=a;
  baseState = document.getElementById(`check_list${origin}`).checked;
  for(var i=0; i<listOrderChanges.length; i++){
    el = document.getElementById(`check_list${a}`);
    el.checked = baseState;
    a = listOrderChanges[a];
    if(a==origin) break;
  }
}

// ========================================================================== //

function review(){
  // load songs that have changes waiting.
  for(var k in songChanges){
    var i = k.split(',');
    // console.log(k, key, songChanges[k][key]);
    if(!songs[i[0]]){
      loadSong( `${dataFolder}${listData[i[0]].uid}.json`, i[0], review);
      return;
    }
  }
  for(var k=0; k<authorChanges.length; k++){
    if(!authorChanges[k]) continue;
    if(!songs[k]){
      loadSong( `${dataFolder}${listData[k].uid}.json`, k, review);
      return;
    }
  }
  for(var k=0; k<notesChanges.length; k++){
    if(!notesChanges[k]) continue;
    if(!songs[k]){
      loadSong( `${dataFolder}${listData[k].uid}.json`, k, review);
      return;
    }
  }

  listOrderChangesString="";
  titleChangesString="";
  authorChangesString="";
  notesChangesString="";
  songChangesString="";

  check=`<label>
    <input type=checkbox id="check$i" checked=true>`
  reconcile=` onclick="reconcileListOrderChain($i)">`;

  for(var i=0; i<listOrder.length; i++){
    j=listOrder[i];
    if(j!=i){
      listOrderChangesString+=check.replace('$i',`_list${i}`)
        .replace('>', reconcile.replace('$i',i) );
      listOrderChangesString+=`${listData[j].title
      } [<s>${listData[j].uid}</s>&rarr;${listData[j].new_uid}]</label><br>`;
      listOrderChanges[i]=j;
    }
  }

  for(var i=0; i<titleChanges.length; i++){
    j=listOrder[i];
    if(titleChanges[j]){
      titleChangesString+=check.replace('$i',`_title${j}`);
      titleChangesString+=`<s>${listData[j].title
      } <small>[${listData[j].uid}]</small></s>
      &rarr; ${titleChanges[j]} <small>[${listData[j].new_uid}]</small></label><br>`;
    }
  }

  for(var i=0; i<authorChanges.length; i++){
    j=listOrder[i];
    if(authorChanges[j]){
      authorChangesString+=check.replace('$i',`_author${j}`);
      authorChangesString+=`<s>${songs[j].author}</s> &rarr; ${authorChanges[j]}</label><br>`;
    }
  }

  for(var i=0; i<notesChanges.length; i++){
    j=listOrder[i];
    if(notesChanges[j]){
      notesChangesString+=check.replace('$i',`_notes${j}`);
      notesChangesString+=`<s>${songs[j].notes.join()}</s> &rarr; ${notesChanges[j]}</label><br>`;
    }
  }

  for(var k in songChanges){
    var i = k.split(',');
    // var key = songChanges[k].key;

    for(key in legend){
      if(songChanges[k][key]){
        var wordFallback = "test";
        if(key=='o'){
          wordFallback=songs[i[0]].verses[i[1]].lines[i[2]][i[3]].w;
          wordFallback=transliterate(wordFallback, songs[i[0]].language);
        }

// console.log(i,'---',i[0],i[1],i[2],i[3],key);
        var wordBase=songs[i[0]].verses[i[1]].lines[i[2]][i[3]][key];
        songChangesString+=check.replace('$i',`_song${k}-${legend[key]}`)
        + `${listData[ i[0] ].title}
          <small>[${listData[ i[0] ].new_uid} ▶
          verse ${i[1]*1+1} ▶ line ${i[2]*1+1} ▶ word ${i[3]*1+1} ▶ ${legend[key]}]:</small>
          <s>${
            key=='s'||key=='o'
            ?(wordBase[language]==undefined && key=='o' ? wordFallback : wordBase[language])
            : wordBase}</s>
          &rarr; ${songChanges[k][key]}</label><br>`;
          // console.log(songChanges[k][key]);
      }
    }

    if(songChanges[k].translation)
      songChangesString+=check.replace('$i',`_song${k}-translation`)
      + `${listData[ i[0] ].title}
        <small>[${listData[ i[0] ].new_uid} ▶
        verse ${i[1]*1+1} ▶ translation]:</small>
        <br>${renderTransChange(
          songs[i[0]].verses[i[1]].translation[language],
          songChanges[k].translation
        )}</label><br>`;
        // <s>${songs[i[0]].verses[i[1]].translation}</s>
        // <br>&rarr;<br>${songChanges[k].translation}
  }

  document.getElementById('reViewer').innerHTML=
   `${listOrderChangesString?`<h2>Re-ordering</h2>${listOrderChangesString.replace(/\[comma\]/g, ",")}<hr><br>`:''}
    ${titleChangesString?`<h2>Title Changes</h2>${titleChangesString.replace(/\[comma\]/g, ",")}<hr><br>`:''}
    ${authorChangesString?`<h2>Author Changes</h2>${authorChangesString.replace(/\[comma\]/g, ",")}<hr><br>`:''}
    ${notesChangesString?`<h2>Notes Changes</h2>${notesChangesString.replace(/\[comma\]/g, ",")}<hr><br>`:''}
    ${songChangesString?`<h2>Song Changes</h2>${songChangesString.replace(/\[comma\]/g, ",")}`:''}`;
  document.getElementById('blackout').style.display='block';
}

// ========================================================================== //

function renderTransChange(a,b){
  removed = getDifference(a,b);
  added = getDifference(b,a);

  var j=0;
  var res=''
  for(var i=0; i<removed.length; i++){
    if(removed[i].p!=-1){
      res+=`${removed[i].a} `;
      j++;
    }
    else res+=`<s>${removed[i].a}</s> `;

    if(added[j]){
      while(added[j].p==-1 && j<added.length){
        res+=`<span style='color:#090'>${added[j].a}</span> `;
        j++;
        if(!added[j]) break;
      }
    }

  }

  return res;
}

function getDifference(a, b){
  a=a.split(' ');
  b=b.split(' ');
  var res=[];
  var p=0;
  for(var i=0; i<a.length; i++){
    p = b.indexOf(a[i]);
    res.push({p:p, a:a[i], b:(p>-1?b[p]:'')});
    if(p>-1) b = b.slice(0,p).concat("^").concat(b.slice(p+1));
  }
  return res;
}

// ========================================================================== //

function cancel(){
  e = window.event;
  // console.log(e.target)
  if( !(e.target==document.getElementById('blackout')
    || e.target==document.getElementById('cancel')) ) return;
  document.getElementById('blackout').style.display='none';
}

// ========================================================================== //

function publish(){
  // alert('not yet a real button');
  discard(true);
}

// ========================================================================== //


function discard(publish=false){
  if(!publish) // on discard
    if(!confirm('WARNING! This will permanently discard the selected changes.')) return;

  var outputSongs={};

  // ++++++++++++++++++++++++++++++++++++++++ //

  // delete (and publish?) selected listOrder Changes
  for(var i=0; i<listOrderChanges.length; i++){
    if(!listOrderChanges[i]) continue; // empty slots

    el = document.getElementById(`check_list${i}`);
    if(!el) continue;
    if(!el.checked) continue;

    listOrder[i]=""+i;

    if(publish) outputSongs[i]={uid:listData[i].new_uid};
    delete listOrderChanges[i];
  }
  localStorage.gkListOrder = listOrder.join();

  // ++++++++++++++++++++++++++++++++++++++++ //

  // delete (and publish?) selected title Changes
  for(var i=0; i<titleChanges.length; i++){
    if(!titleChanges[i]){ delete titleChanges[i]; continue; } // empty slots

    el = document.getElementById(`check_title${i}`);
    if(!el) continue;
    if(!el.checked) continue;

    if(publish){
      if(!outputSongs[i]) outputSongs[i]={};
      outputSongs[i].title=titleChanges[i];
    }
    delete titleChanges[i];
  }

  localStorage.gkTitleChanges = titleChanges.join();

  // ++++++++++++++++++++++++++++++++++++++++ //

  // delete (and publish?) selected author Changes
  for(var i=0; i<authorChanges.length; i++){
    if(!authorChanges[i]) continue; // empty slots

    el = document.getElementById(`check_author${i}`);
    if(!el) continue;
    if(!el.checked) continue;

    if(publish){
      if(!outputSongs[i]) outputSongs[i]={};
      outputSongs[i].author=authorChanges[i];
    }
    delete authorChanges[i];
  }
  localStorage.gkAuthorChanges = authorChanges.join();

  // ++++++++++++++++++++++++++++++++++++++++ //

  // delete (and publish?) selected notes Changes
  for(var i=0; i<notesChanges.length; i++){
    if(!notesChanges[i]) continue; // empty slots

    el = document.getElementById(`check_notes${i}`);
    if(!el) continue;
    if(!el.checked) continue;

    if(publish){
      if(!outputSongs[i]) outputSongs[i]={};
      outputSongs[i].notes=notesChanges[i];
    }
    delete notesChanges[i];
  }
  localStorage.gkNotesChanges = notesChanges.join();

  // ++++++++++++++++++++++++++++++++++++++++ //

  // delete (and publish?) selected song Changes... (now NO translations)
  for(var i in songChanges){
    // prepare some keys and objects to store updated songChanges in
    if(publish){
      var splitKey = i.split(",");
      var songNumber = splitKey.shift();
      var verseAddress = splitKey.join(",");
      if(!outputSongs[songNumber]) outputSongs[songNumber]={words:{}};
      if(!outputSongs[songNumber].words) outputSongs[songNumber].words={};
    }

    // multiple types of checkboxes for this one
    for(key in legend){
      el = document.getElementById(`check_song${i}-${legend[key]}`);
      if(!el) continue;
      if(!el.checked) continue;

      if(publish){
        if(!outputSongs[songNumber].words[verseAddress])
          outputSongs[songNumber].words[verseAddress]={};
        outputSongs[songNumber].words[verseAddress][key]=songChanges[i][key];
      }
      delete songChanges[i][key];
    }
    //check for empty song
    if(Object.entries(songChanges[i]).length === 0) delete songChanges[i];
  }

  // ++++++++++++++++++++++++++++++++++++++++ //

  // delete (and publish?) selected song Changes... (now ONLY translations)
  for(var i in songChanges){
    el = document.getElementById(`check_song${i}-translation`);
    if(!el) continue;
    if(!el.checked) continue;

    if(publish){
      var splitKey = i.split(",");
      var songNumber = splitKey[0];
      var verseNumber = splitKey[1];

      if(!outputSongs[songNumber]) outputSongs[songNumber]={translations:{}};
      if(!outputSongs[songNumber].translations) outputSongs[songNumber].translations={};
      outputSongs[songNumber].translations[verseNumber] = songChanges[i].translation;
    }
    delete songChanges[i];
  }
  localStorage.gkSongChanges = JSON.stringify(songChanges);

  // ++++++++++++++++++++++++++++++++++++++++ //

  // console.log('songChanges:',songChanges);
  // console.log('outputSongs:',outputSongs);

  if(publish && !false){
    var outputSongs_uidBased={}
    for(var i in outputSongs) outputSongs_uidBased[listData[i].uid] = outputSongs[i];
    // console.log(outputSongs);
    // console.log(outputSongs_uidBased);
    var outputSongsString=JSON.stringify(outputSongs_uidBased);
    localStorage.gkChangesBackup=outputSongsString;
    saveData(outputSongsString.replace(/\&nbsp\;/g, " ").replace(/\[comma\]/g, ","));
  }
  else location.href=location.href;
}
