
// ========================================================================== //

function addSongView(){
  // document.getElementById('adder').innerHTML="Paste here...";
  adderPreviewUpdate();
  document.getElementById('blackout_addSong').style.display='block';
}

// ========================================================================== //

function adderPreviewUpdate(){
  data=document.getElementById('adder').value;
  document.getElementById('adderPreview').innerHTML=renderPreview(raw2json(data));
}

function renderPreview(s){
  var HTML=`<h3>${s.title}</h1>`;
  // console.log(s);
  for(var v=0; v<s.verses.length; v++){
    var Beng="";
    var Roma="";
    var Syno="";
    var Tran="";
    if(s.verses[v].lines.length==1)
      if(s.verses[v].lines[0].length==0) continue;
    for(var l=0; l<s.verses[v].lines.length; l++){
      for(var w=0; w<s.verses[v].lines[l].length; w++){
        Beng+=`<span class=w>${s.verses[v].lines[l][w].w}</span>`;
        Beng+=`<span class=h>${s.verses[v].lines[l][w].h}</span>`;
        Roma+=`<span class=w>${transliterate(s.verses[v].lines[l][w].w, s.language)}</span>`;
        Roma+=`<span class=h>${transliterate(s.verses[v].lines[l][w].h, s.language)}</span>`;
        Syno+=`<span class=w>${s.verses[v].lines[l][w].w}</span>`;
        Syno+=`—<span class=s>${s.verses[v].lines[l][w].s[language]}</span>; `;
      }
      Beng+="\n"
      Roma+="\n"
      Syno+="\n"
    }
    try{ Tran=`<span class=tran>${s.verses[v].translation[language]}</span>`; }catch(e){}
    HTML+=Beng+"\n"+Roma+"\n<p style='font-size:8px;'>"+Syno+"\n"+Tran+"</p>\n\n<hr>";
  }
  return HTML;
}

// ========================================================================== //

function cancel_addSong(){
  e = window.event;
  // console.log(e.target)
  if( !(e.target==document.getElementById('blackout_addSong')
    || e.target==document.getElementById('cancel_add')) ) return;
  document.getElementById('blackout_addSong').style.display='none';
}

// ========================================================================== //

function addSong(){
  if(document.getElementById('uidLetterAdd').innerHTML==""){ alert("need a letter :P"); return; }
  if(!confirm('WARNING! This will permanently add this song and update the song list.')) return;

  rawSong = document.getElementById('adder').value;
  uidG = document.getElementById('uidLetterAdd').innerHTML;
  outputSongsString=JSON.stringify({completeSong:raw2json(rawSong),uidGroup:uidG});
  saveData(outputSongsString);
}


var testData=`নিতাই আমার পরম দয়াল ।
আনিয়া প্রেমের বন্যা, জগত করিল ধন্যা,
ভরিল প্রেমেতে নদী খাল ॥ধ্রু॥

লাগিয়া প্রেমের ঢেউ, বাকি না রহিল কেউ,
পাপী-তাপী চলিল ভাসিয়া ।
সকল ভকত মেলি, সে প্রেমেতে করে কেলি,
কেহ কেহ যায় সাঁতারিয়া ॥

ডুবিল নদীয়াপুর, ডুবে প্রেমে শান্তিপুর,
দোহে মিলি বাইছ খেলায় ।
তা দেখি নিতাই হাসে, সকলেই প্রেমে ভাসে,
বাসু ঘোষ হাবুডুবু খায় ॥

স্বামী
গোস্বামী স্বামী`;
