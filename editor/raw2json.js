var separators=",-—!;  ০১২৩৪৫৬৭৮৯।॥";

function raw2json(raw){
  raw=raw.replace(/।।/g,"॥");
  var song={};
  var s_in  = raw.split("\n");
  var s_out = [{lines:[[]]}];
  var v=0;
  var l=0;
  for(var j=0; j<s_in.length; j++){
    if(s_in[j]=="") continue;
    // split into words with separator hints
    var word="";
    var hint="";
    var hintMode=false;
    for(var k=0; k<s_in[j].length; k++){
      c = s_in[j][k];

      if(separators.indexOf(c)!=-1){
        hintMode=true;
        hint+=c;
      } else if (hintMode) {
        hintMode=false;
        s_out[v].lines[l].push({w:word,h:hint,s:{eng:'NULL'},o:{}});
        word=c;
        hint="";
      } else
      word+=c;
    }
    s_out[v].lines[l].push({w:word,h:hint,s:{eng:'NULL'},o:{}});
    s_out[v].translation={eng:"UNDEFINED"}
    if(s_in[j].substr(-1)=='॥'){
      s_out[++v]={lines:[[]]};
      l=0;
    } else s_out[v].lines[++l]=[];
  }
  song.uid = "UID?"
  song.title = s_in[0];
  song.author = "?";
  song.language = "";
  song.notes = [];
  song.audio = {};
  song.has_synonyms = false;
  song.has_translations = false;
  song.legend = {w:"word",h:"hint",s:"synonym",o:"override"};
  song.verses = s_out;
  return song;
}
