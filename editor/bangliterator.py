import re

bn_ind_vwl = ['অ', 'আ', 'ই', 'ঈ', 'উ', 'ঊ', 'ঋ', 'এ', 'ঐ', 'ও', 'ঔ']
bn_depend_vwl = ['া', 'ি', 'ী', 'ু', 'ূ', 'ৃ', 'ে', 'ৈ', 'ো', 'ৌ']
bn_con = ['ক', 'খ', 'গ', 'ঘ', 'ঙ', 'চ', 'ছ', 'জ', 'ঝ', 'ঞ', 'ট', 'ঠ', 'ড', 'ঢ', 'ণ', 'ড়', 'ঢ়', 'ত', 'থ', 'দ', 'ধ', 'ন', 'প', 'ফ', 'ব', 'ভ', 'ম', 'য', 'য়', 'র', 'ল', 'শ', 'ষ', 'স', 'হ']
bn_ind_vwl = '|'.join(bn_ind_vwl)
bn_depend_vwl = '|'.join(bn_depend_vwl)
bn_con = '|'.join(bn_con)

interpuncterfy = ''.join(bn_ind_vwl + '|' + bn_depend_vwl + '|' + bn_con + '|ঁ')

trans_ind_vwl_n = {'অঁ':'ã', 'আঁ':'ā̃', 'ইঁ':'ĩ', 'ঈঁ':'ī̃', 'উঁ':'ũ', 'ঊঁ':'ū̃', 'এঁ':'ẽ', 'ঐঁ':'aĩ', 'ওঁ':'o', 'ঔঁ':'aũ'}
trans_ind_vwl = {'অ':'a', 'আ':'ā', 'ই':'i', 'ঈ':'ī', 'উ':'u', 'ঊ':'ū', 'ঋ':'ṛ', 'এ':'e', 'ঐ':'ai', 'ও':'o', 'ঔ':'au', 'ং':'ṁ', 'ঃ':'ḥ'}
trans_dpd_vwl_n = {'াঁ':'ā̃', 'িঁ':'ĩ', 'ীঁ':'ī̃', 'ুঁ':'ũ', 'ূঁ':'ū̃', 'েঁ':'ẽ', 'ৈঁ':'aĩ', 'োঁ':'o', 'ৌঁ':'aũ'}
trans_dpd_vwl = {'া':'ā', 'ি':'i', 'ী':'ī', 'ু':'u', 'ূ':'ū', 'ৃ':'ṛ', 'ে':'e', 'ৈ':'ai', 'ো':'o', 'ৌ':'au'}

trans_con = {'ক্':'k', 'কঁ':'kã', 'ক':'ka', 'খ্':'kh', 'খঁ':'khã', 'খ':'kha', 'গ্':'g', 'গঁ':'gã', 'গ':'ga', 'ঘ্':'gh', 'ঘঁ':'ghã','ঘ':'gha', 'ঙ্':'ṅ','ঙ':'ṅa',
             'চ্':'c', 'চঁ':'cã', 'চ':'ca', 'ছ্':'ch', 'ছঁ':'chã', 'ছ':'cha', 'জ্':'j', 'জঁ':'jã', 'জ':'ja', 'ঝ্':'jh', 'ঝঁ':'jhã', 'ঝ':'jha', 'ঞ্':'ñ', 'ঞ':'ña',
             'ত্':'t', 'তঁ':'tã', 'ত':'ta', 'থ্':'th', 'থঁ':'thã', 'থ':'tha', 'দ্':'d', 'দঁ':'dã', 'দ':'da', 'ধ্':'dh', 'ধঁ':'dhã', 'ধ':'dha', 'ন্':'n', 'নঁ':'nã', 'ন':'na', 'ৎ':'t',
             'ট্':'ṭ', 'টঁ':'ṭã', 'ট':'ṭa', 'ঠ্':'ṭh', 'ঠঁ':'ṭhã', 'ঠ':'ṭha', 'ড্':'ḍ', 'ডঁ':'ḍã', 'ড':'ḍa', 'ঢ':'ḍh', 'ঢঁ':'ḍhã', 'ঢ':'ḍha', 'ণ্':'ṇ', 'ণ':'ṇa', 'ড়্':'ɽ', 'ড়ঁ':'ɽã', 'ড়':'ɽa', 'ঢ়্':'ɽh', 'ঢ়ঁ':'ɽhã', 'ঢ়':'ɽha',
             'প্':'p', 'পঁ':'pã', 'প':'pa', 'ফ্':'ph', 'ফঁ':'phã', 'ফ':'pha', 'ব্':'b', 'বঁ':'bã', 'ব':'ba', 'ভ্':'bh', 'ভঁ':'bhã', 'ভ':'bha', 'ম্':'m', 'মঁ':'mã', 'ম':'ma',
             'য্':'y', 'যঁ':'yã', 'য':'ya', 'য়্':'ẏ', 'য়ঁ':'ẏã', 'য়':'ẏa', 'র্':'r', 'রঁ':'rã', 'র':'ra', 'ল্':'l', 'লঁ':'lã', 'ল':'la',
             'শ্':'ś', 'শঁ':'śã', 'শ':'śa', 'ষ্':'ṣ', 'ষঁ':'ṣã', 'ষ':'ṣa', 'স্':'s', 'সঁ':'sã', 'স':'sa', 'হ্':'h', 'হঁ':'hã', 'হ':'ha'}



str = input(u"enter test string: ")



#0 interpunct function!!!!!!!!!!!!!!
#1 trans_ind_vwl_n
#2 trans_ind_vwl
#3 if con followed by dependant vowel then con = con + halant
#4 trans_dpd_vwl_n
#5 trans_dpd_vwl
#6 trans_con
#7 j_fola issue


def transliterate(str):
    pattern = r'(' + interpuncterfy + ')(' + bn_ind_vwl + ')'  # interpunct function (puts a dot between these; তাই ওই এই ওউ)
    str = re.sub(pattern, lambda m: m.group(1) + '·' + m.group(2), str)
    for key in trans_ind_vwl_n.keys():
        str = str.replace(key, trans_ind_vwl_n[key]) #2
    for key in trans_ind_vwl.keys():
        str = str.replace(key, trans_ind_vwl[key]) #3
    pattern = r'(' + bn_con + ')(' + bn_depend_vwl + ')'
    str = re.sub(pattern, lambda m: m.group(1) + '্' + m.group(2), str)  #3  if con followed by dependant vowel then con = con + halant
    for key in trans_dpd_vwl_n.keys():
        str = str.replace(key, trans_dpd_vwl_n[key]) #4
    for key in trans_dpd_vwl.keys():
        str = str.replace(key, trans_dpd_vwl[key]) #5
    for key in trans_con.keys():
        str = str.replace(key, trans_con[key]) #6
    pattern = r'([kgṅcjñṭḍṇtdnpbmyrlśṣsh])(y)'  #7 j_fola issue
    str = re.sub(pattern, lambda m: m.group(1) + 'ẏ', str)
    return str



print(transliterate(str))


