
function updateViewer(n){
  for(var p=0; p<listData[n].new_uid.length; p++)
    if( !isNaN(listData[n].new_uid[p]) ) break;
  listData[n].letter = listData[n].new_uid.substr(0,p);
  listData[n].number = listData[n].new_uid.substr(p);

  var title = listData[n].title;
  var author = songs[n].author;
  var notes = songs[n].notes.join('\n');
  if(titleChanges[n]) title = titleChanges[n].replace(/\[comma\]/g, ",");
  if(authorChanges[n]) author = authorChanges[n].replace(/\[comma\]/g, ",");
  if(notesChanges[n]) notes = notesChanges[n].replace(/\[comma\]/g, ",");
  if(author=="?") author={eng:"Unknown Author", ben:"অজানা লেখক"}[language];

  songtitle.innerHTML = mode.roma?transliterate(title, songs[n].language):title;
  songauthor.innerHTML = mode.roma?transliterate(author, songs[n].language):author;
  songnotes.innerHTML = mode.roma?transliterate(notes, songs[n].language):notes;
  uidLetter.innerHTML = songs[n].uid;
  uidNumber.innerHTML = "&rarr;"+listData[n].number;

  fileviewer.innerHTML = "";

  var i=0;
  count=0;

  if(!songs[n].verses){
    console.log("bad song");
    fileviewer.innerHTML = "song file is incorrectly formatted";
  }
  else{
    if(!updateViewerOffset) updateViewerOffset=0;
    for(var v=updateViewerOffset; v<=songs[n].verses.length; v++){

      if(appendPrevNext(n,v)) return;
      if(v==songs[n].verses.length) return;

      fileviewer.innerHTML += renderVerse(n,v);
    }
  }
}

// ========================================================================== //

function renderVerse(n,v){
  verseHTML=(v*1+1)+"<div class=verse>";
  verseHTML+= renderBang(n,v)+renderRoma(n,v)+renderSyno(n,v)+renderTran(n,v);
  verseHTML+= "</div>";
  return verseHTML;
}

function utility(el, task, n,v){
  bang=el.parentElement.nextSibling;
  if(task=='note'){
    songnotes.innerHTML+='\n'+bang.innerText;
    songnotes.onkeyup();
  }
  if(task=='xout' || task=='note'){ // if you move it to note, also clear the verse
    for(var i=0; i<bang.children.length; i++){
      bang.children[i].innerHTML="x";
      try{ bang.children[i].onkeyup(); }catch(e){};
    }
  }
  if(task=='undo'){
    var l=w=0;
    for(var i=0; i<bang.children.length; i++){
      // if(bang.children[i].innerHTML!='x') continue;
      if(bang.children[i].innerHTML=='<br>') continue;
      if(bang.children[i].getAttribute('class')==null) continue;
      if(bang.children[i].getAttribute('class').indexOf('w')>-1)
        bang.children[i].innerHTML=songs[n].verses[v].lines[l][w].w;

      if(bang.children[i].getAttribute('class').indexOf('h')>-1){
        bang.children[i].innerHTML=songs[n].verses[v].lines[l][w].h;
        w++; if(w==songs[n].verses[v].lines[l].length){ l++; w=0; }
      }

      try{ bang.children[i].onkeyup(); }catch(e){};
    }
  }
}

function renderBang(n,v, utility=true){
  if(!mode.bang) return "";

  if(utility)
    HTML = `
      <div class="utilityBar">
        <span class="utility" onclick="utility(this,'note')">⤒</span>
        <span class="utility" onclick="utility(this,'xout')">⌫</span>
        <span class="utility" onclick="utility(this,'undo',${n},${v})">⟲</span>
      </div>`;
  else HTML="";

  HTML += "<div class=bang>";
  for(var l=0; l<songs[n].verses[v].lines.length; l++){
    for(var w=0; w<songs[n].verses[v].lines[l].length; w++){
      HTML += addField(n,v,l,w, 'w'); // w=WORD
      HTML += addField(n,v,l,w, 'h');  // h=HINT
    }
    HTML += '<br>';
  }
  HTML += '</div>';
  return HTML;
}

function renderRoma(n,v){
  if(!mode.roma) return "";
  HTML = '<div class=roma>';
  for(var l=0; l<songs[n].verses[v].lines.length; l++){
    for(var w=0; w<songs[n].verses[v].lines[l].length; w++){
      var W = addField(n,v,l,w, mode.over?'o':'w-t', bold='', frozen=!mode.over); // o=OVERRIDE
      var H = addField(n,v,l,w, 'h-t'); // h=HINT
      HTML += W;
      HTML += H;
    }
    HTML += '<br>';
  }
  HTML += '</div>';
  return HTML;
}

function renderSyno(n,v){
  if(!mode.syno) return "";

  HTML = '<div class=syno>';
  for(var l=0; l<songs[n].verses[v].lines.length; l++){
    for(var w=0; w<songs[n].verses[v].lines[l].length; w++){
      var W = addField(n,v,l,w, (mode.roma&&mode.over)?'o':mode.roma?'w-t':'w', bold=true, frozen=true); // o=OVERRIDE
      var S = addField(n,v,l,w, 's')
      if(W=="") continue;

      retro=(S==-2?2:(S==-1?1:0));
      var burnt = false;
      for(var r=0; r<retro; r++){
        console.log(retro);
        var word = songs[n].verses[v].lines[l][w].w;
        if(mode.roma) word = transliterate(word, songs[n].language);
        var fields=HTML.split("\n  >");
        var prev = fields.length-2;
        fieldsPrev=fields[prev].split("</span");
        fields[prev]=`${fieldsPrev[0]}-${word}</span${fieldsPrev.slice(1)}`;
        HTML = fields.join("\n  >");
        // if(!burnt) console.log(fields.join("\n  >"));
        burnt=true;
      } if(burnt) continue;

      HTML += W;
      HTML += "&mdash;";
      HTML += S; // s=SYNONYM
      HTML += ";&nbsp;&nbsp;";
    }
    HTML += '<br>';
  }
  HTML += '</div>';
  return HTML;
}

function renderTran(n,v){
  if(!mode.tran) return "";
  // console.log(songs[n].verses[v]);
  var tranText = songs[n].verses[v].translation[language];
  var change=false;
  if(songChanges[`${n},${v},tran`])
    change = songChanges[`${n},${v},tran`].translation;
  edited = false;
  if(change) edited = (tranText != change);
  if(edited) tranText = change;
  HTML = `<div class=tran><p class="translation${edited?' edited':''}"
    onkeyup="tranFieldChange(${n},${v},this)"
    contenteditable=true>${tranText}</p></div>`;

  return HTML;
}

// ========================================================================== //

function addField(n,v,l,w, key, bold='', frozen=false){
  var realKey=key;
  if(key=="w-t") realKey='w';
  if(key=="h-t") realKey='h';
  var scKey=[n,v,l,w].join(',');
  var cssClass = realKey+(bold?" bold":"")+(frozen?" frozen":"");
  var wordGroup=songs[n].verses[v].lines[l][w];
  var text = wordGroup[realKey];

  if(key=='s') text = wordGroup.s[language];
  if(key=='o'){
    if(wordGroup.o[language]) text = wordGroup.o[language];
    else{
      text = wordGroup.w;
    }
  }

  if(songChanges[scKey]){
    if(key=='o' && !songChanges[scKey][realKey]) key='w';
    // if(key=='o')
    //   if(songChanges[scKey].o && mode.over){
    //     cssClass += " edited";
    //     text = songChanges[scKey].o;
    //   }
    // else
    if(songChanges[scKey][realKey]){
      cssClass += " edited";
      text = songChanges[scKey][realKey];
    }
  }

  if(text=="") return "";
  // if(key=='s' && text=="⬅1") return -1;
  // if(key=='s' && text=="⬅2") return -2;
  if(key=="w-t") text=transliterate(text, songs[n].language);
  if(key=="h-t") text=transliterate(text, songs[n].language);
  var fieldHTML=`<span class="${cssClass}" contenteditable=${!frozen?"true":"false"}
    onkeyup='fieldChange("${n},${v},${l},${w}", "${cssClass}", "${realKey}", this)'
  >${text}</span>`;
  count++;
  return fieldHTML;
}

// ========================================================================== //

function appendPrevNext(n,v){
  if(count>max || (v==songs[n].verses.length && lastV.length>0)){
    fileviewer.innerHTML +=
      `[Truncated] Showing verses ${updateViewerOffset} through ${v} of ${songs[n].verses.length}`
      +`<br><br><hr><br>
      ${lastV.length>0?`<div
        class='prevNext button'
        onclick="updateViewerOffset = lastV.pop(); updateViewer(${n}); "
      ><span style='font-family:Comic Sans MS;'>&#9664;</span> Prev</div>`:''}
      ${count>max?`<div
        class='prevNext button'
        onclick="lastV.push(${updateViewerOffset}); updateViewerOffset = ${v}; updateViewer(${n})"
      >Next <span style='font-family:Comic Sans MS;'>&#9654;</span></div>`:''}
      <br><br><br>`;
    if(updateViewerOffset>0) fileviewer.parentElement.scroll(0,999999);
    return true;
  }
  return false;
}
