var HTTP_LOG = false;

var dataFolder = "../json/";
var outputFolder = "../json/"; // this means it's LIVE
// var outputFolder = "../json-output/"; // this is for testing
var backupFolder = "../json-bak/";

var http = require('http');
var url = require('url');
var fs = require('fs');
var pr = require("./pretty.js");
var allow = "Access-Control-Allow-Origin";
var webPort = 80;
var legend={w:"bangla     ",h:"hint       ",s:"synonym    ",o:"override   "};

http.createServer(function (req, res) {
  var q = url.parse(req.url, true);
  var filename="";
  if( q.pathname=="/" ) filename = "./index.htm";  // whatever you want instead of index.htm
  else filename = `.${q.pathname}`;
  ext = filename.split('.').pop();
  if( ext=='json' || ext=='col' ) filename='.'+filename;
  console.log({ ReqURL:req.url, Fn:filename, Ext:ext });

  //vars.varname ...
  var vars = url.parse(req.url, true).query;
  for(v in vars){ print_( ' ' , { key:v, val:vars[v]}); }

  // POST
  if(req.method == 'POST') {
    var body = '';
    req.on('data', function(data){ body += data; });
    req.on('end', function() {
      // do something with body;
      allChanges=JSON.parse(body);
      commit(allChanges);
      print_('',{
        PostTarget:filename,
        PostBody:allChanges
      });
      res.writeHead(200, {'Content-Type': 'text/html'});
      res.end('Data received.');
    });
  }

  else { // GET
    res.writeHead(200, {'Content-Type': 'text/html'});
    fs.readFile(filename, function(err, data) {
      if (err) {
        res.writeHead(404, {'Content-Type': 'text/html', allow : "*"});
        return res.end("404 Not Found");
      }
      res.writeHead(200, {'Content-Type': (ext=='css'?'text/css':'text/html'), allow : "*"});
      res.write(data);
      return res.end();
    });
  }

}).listen(webPort);

let now = new Date();
console.log('\n\n\n------------------------------------------------------------------------\n');
console.log([now.toString()]);
log(`  Gaudiya Kirtan Content Server:\n    listening on port ${webPort}\n\n`);



function loadFile(fn) {
  var data = fs.readFileSync(fn, 'utf8');
  return JSON.parse(data.toString());
}

function commit(allChanges){
  // we need to update the info in list.json to match new titles and uid's
  var songList = loadFile(`${dataFolder}_list.json`);
  var newSongList={};

  for(var i=0; i<songList.length; i++)
    newSongList[songList[i].uid]={
      index:i,
      uid:songList[i].uid.toUpperCase(),
      md5:songList[i].md5,
      language:songList[i].language,
      title:cleanTitle(songList[i].title),
      author:songList[i].author.trim()
    };

  let now = new Date();
  log([now.toString()]);

  // backup list file
  fs.writeFileSync(`${backupFolder}_list.json`, pr.prettyList(JSON.stringify(songList)) );
  log(`Backed up "${dataFolder}_list.json" to "${backupFolder}_list.json"`);

  // ADD SONG
  if(allChanges.completeSong){
    log('complete Song found');
    var song = allChanges.completeSong;
    var uidGroup = allChanges.uidGroup.toUpperCase();
    var uidIndex = 1;

    // log(song,uidGroup);
    for(i in songList)
      if(songList[i].uid.substr(0,uidGroup.length)==uidGroup
      && !isNaN(songList[i].uid[uidGroup.length]) ){
        uidIndex++;
        prevIndex=i;
      }

    song.uid=uidGroup.toUpperCase()+uidIndex;
    song.title=cleanTitle(song.title);
    if(song.title.substr(-1)==",") song.title=song.title.substr(0,song.title.length-1);

    var j=0;
    for(var i in newSongList){
      songList[j]={
        uid:newSongList[i].uid,
        md5:newSongList[i].md5,
        language:newSongList[i].language,
        title:cleanTitle(newSongList[i].title),
        author:newSongList[i].author
      };
      if(j==prevIndex) songList[++j]={
        uid:song.uid,
        md5:'', // MD5 checksum?
        language:'',
        title:cleanTitle(song.title),
        author:song.author
      };
      j++;
    }

    fs.writeFileSync(`${outputFolder}${song.uid}.json`, pr.pretty(JSON.stringify(song)) );
    fs.writeFileSync(`${outputFolder}_list.json`, pr.prettyList(JSON.stringify(songList)) );
    log();
    log(`Saved updated file: "${outputFolder}${song.uid}.json"`);
    log(`Saved updated file: "${outputFolder}_list.json"\n`);
    log();
    return;
  }

  // CHANGE SONG(S)
  // backup all effected songs FIRST
  var backupList = {};
  for(var uid in allChanges){
    var change = allChanges[uid];
    var fn = uid;

    backupList[uid] = true;

    // next, redundantly backup the file about to be overwritten
    // in case the posted data was somehow incorrectly formed.
    if(change.uid) backupList[change.uid] = true;
  }

  for(uid in backupList){
    var fn = uid;
    var song = loadFile(`${dataFolder}${fn}.json`);
    fs.writeFileSync(`${backupFolder}${fn}.json`, pr.pretty(JSON.stringify(song)) );
    log(`Backed up "${dataFolder}${fn}.json" to "${backupFolder}${fn}.json"`);
  }

  // now parse changes on each song and save...
  for(var uid in allChanges){
    var change = allChanges[uid];
    var fn = uid;
    var song = loadFile(`${backupFolder}${fn}.json`);
    song.title=cleanTitle(song.title);
    if(song.title.substr(-1)==",") song.title=song.title.substr(0,song.title.length-1);

    // working
    if(change.uid){
      fn = change.uid;
      song.uid = fn;
      newSongList[uid].new_uid=fn;
      // log('uid:',uid,'→',newSongList[uid].new_uid);
      print('\n\n------------------------------------', uid, '→', fn, '---');
    } else print('\n\n------------------------------------', uid, '---');

    // working
    if(change.title){
      newSongList[fn].title = cleanTitle(change.title);
      // log('uid:',song.title,'→',newSongList[uid].title);
    }

    if(change.title){ print('title       :',song.title,'→',change.title); song.title=change.title; }
    if(change.author){ print('author      :',song.author,'→',change.author); song.author=change.author; }
    if(change.notes ){ print('notes       :',song.notes,'→',change.notes); song.notes = change.notes.split('\n'); }

    // working
    if(change.words){
      // synonyms and overrides require a language, fallback to 'eng'
      for(key in change.words){
        var splitKey = key.split(",");
        var v = splitKey[0];
        var l = splitKey[1];
        var w = splitKey[2];
        var newWords = change.words[key];
        for(x in newWords){
          oldX = song.verses[v].lines[l][w][x];
          var newX = newWords[x];
          if(x=='s' || x=='o') newX = {eng:newX};
          print(legend[x], ':', oldX, '→', newX);
          song.verses[v].lines[l][w][x] = newX;
        }
      }
    }

    // working
    if(change.translations){
      for(var v in change.translations){
        print('translation :',song.verses[v].translation,'→',{eng:change.translations[v]});
        song.verses[v].translation = {eng:change.translations[v]};
      }
    }

    fs.writeFileSync(`${outputFolder}${fn}.json`, pr.pretty(JSON.stringify(song)) );
    print();
    log(`Saved updated file: "${outputFolder}${fn}.json"`);
  }

  for(var i in newSongList)
    songList[newSongList[i].index]={
      uid:newSongList[i].uid,
      md5:newSongList[i].md5,
      language:newSongList[i].language,
      title:cleanTitle(newSongList[i].title),
      author:newSongList[i].author
    };
  fs.writeFileSync(`${outputFolder}_list.json`, pr.prettyList(JSON.stringify(songList)) );
  log(`Saved updated file: "${outputFolder}_list.json"\n`);

  log();
}

function cleanTitle(t){
  return t
    .replace(/(&nbsp;){1,}/g, ' ')
    .replace(/ ।|’|‘|”|“|/g,"")
    .replace(/\n/g," ")
    .replace(/\s{1,}/g, " ").trim();
}

var print = console.log;
// var log = console.log;

function print_(){  // http server
  for(var i=0; i<arguments.length; i++)
    if(HTTP_LOG) console.warn(arguments[i],null,2);
  // don't bother logging this to file.
}

function log(){
  for(var i=0; i<arguments.length; i++){
    console.warn(arguments[i]);
    console.log(arguments[i]);
  }
}
