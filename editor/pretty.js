function pretty(fileContentString){
  return fileContentString
    .replace('{"uid"'              ,      '{\n  "uid"'              )
    .replace(',"title"'            ,      ',\n  "title"'            )
    .replace(',"author"'           ,      ',\n  "author"'           )
    .replace(',"language"'         ,      ',\n  "language"'         )
    .replace(',"notes"'            ,      ',\n  "notes"'            )
    .replace(',"audio":['          ,      ',\n  "audio":[\n    '    )
    .replace(/}},{"uid"/g          ,      '}},\n    {"uid"'         )
    // .replace('}}],'                ,      '}}\n  ],\n'              )
    .replace(',"has_synonyms"'     ,      ',\n  "has_synonyms"'     )
    .replace(',"has_translations"' ,      ',\n  "has_translations"' )
    .replace(',"legend"'           ,      ',\n  "legend"'           )
    .replace(',"verses"'           ,      ',\n  "verses"'           )
    .replace(/"lines":\[/g         ,      '\n    "lines":[\n      ' )
    .replace(/,"translation"/g     ,      ',\n    "translation"'    )
    .replace(/,\[/g                ,      ',\n      ['              )
    .replace(/\]},{/g              ,      '\n    ]},{'              )
    .replace(',\n      []],'       ,      '],'                      )
    .replace('"audio":[\n    ],\n' ,      '"audio":[],\n'           )
    .replace(/}\]}/g               ,      '}\n  ]\n}'               );
}

function prettyList(listString){
  return listString
    .replace("[{", "[\n  {")
    .replace("}]", "}\n]")
    .replace(/\},\{/g, "},\n  {");
}

function cleanTitle(t){
  return t
    .replace(/(&nbsp;){1,}/g, " ")
    .replace(" ।","")
    .replace(/\n/g," ")
    .replace(/\s{1,}/g, " ").trim();
}

module.exports = { pretty , prettyList , cleanTitle };
