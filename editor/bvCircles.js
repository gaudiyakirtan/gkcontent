var npForbv=[], bvIndex=-1;
var bangFieldsForBV;

function findAllBV(){
  bangFieldsForBV=document.getElementsByClassName('w');
  npForbv=[];
  for(var b in bangFieldsForBV){ if(isNaN(b)) continue;
    t=bangFieldsForBV[b].innerText;
    if(t.search("ৱ")!=-1){
      t=t.replace(/ৱ/g, "[ৱ]")
        .replace(/\[\[ৱ\]\]/g, "[ৱ]")
        .replace(/\[\]/g, "").replace(/\[ব\]/g, "ব");
      bangFieldsForBV[b].innerText=t;
      for(var c in t){
        if(t[c]=="ৱ"){
          bangFieldsForBV[b].style.border="solid 5px rgba(255,0,0,.4)";
          bangFieldsForBV[b].style.borderRadius="5px";
          bangFieldsForBV[b].style.background="rgba(255,128,128,.2)";
          npForbv.push({n:b, p:c});
        }
      }
    }
    else{
      bangFieldsForBV[b].innerText=bangFieldsForBV[b].innerText
        .replace(/\[\]/g, "").replace(/\[ব\]/g, "ব");
      bangFieldsForBV[b].style.border="none";
      bangFieldsForBV[b].style.borderRadius="none";
      bangFieldsForBV[b].style.background="auto";
    }
  }
}

document.onmouseup=function(e){
  var sel = window.getSelection();
  var currentNode = sel.focusNode.parentElement;
  // console.log(bvIndex);

  if(bangFieldsForBV[npForbv[bvIndex].n]!=currentNode){
    var currentNodeP=currentNode;
    var currentNodeN=currentNode;
    var matchIndex = -1;
    var maxOut = 0;
    while(matchIndex<0 && maxOut<108){
      for(var b in bangFieldsForBV){ if(isNaN(b)) continue;
        if(bangFieldsForBV[b] == currentNodeN
        || bangFieldsForBV[b] == currentNodeN){
          if(bangFieldsForBV[b].innerText.search("ৱ")!=-1){
            matchIndex = b;
            break;
          }
        }
      }
      currentNodeP = currentNodeP.previousElementSibling;
      currentNodeN = currentNodeN.nextElementSibling;
      maxOut++;
    }
    // console.log("next match:",matchIndex, "@matchIndex",bangFieldsForBV[matchIndex]);

    // find bvIndex with n and p with matching innerText
    var n,p;
    for(var i in npForbv){ if(isNaN(i)) continue;
      n=npForbv[i].n;
      p=npForbv[i].p;
      if(bangFieldsForBV[n]==bangFieldsForBV[matchIndex]){
        bvIndex = i-1;
        break;
      }
    }
    // console.log(bvIndex);
  }
};

document.onkeyup=function(e){
  if(e.altKey && e.key=="Tab"){
    e.preventDefault();
    // findAllBV();
    // console.log(npForbv.length);
    var currentNode;
    var sel = window.getSelection();

    if(npForbv.length==0){
      currentNode = fileviewer.children[0].children[1].children[0];
      findAllBV();
    }
    else currentNode = sel.focusNode.parentElement;

    // console.log(npForbv[bvIndex].n,npForbv[bvIndex].p);

    var range = new Range();
    var n,p;

    bvIndex+=e.shiftKey?-1:1;
    if(bvIndex<0) bvIndex=npForbv.length-1;
    if(bvIndex>npForbv.length-1) bvIndex=0;

    n=npForbv[bvIndex].n;
    p=npForbv[bvIndex].p;
    bangFieldsForBV[n].focus();
    var node=bangFieldsForBV[n].firstChild;

    range.setStart(node, p);
    range.setEnd(node, p*1+1);
    sel.removeAllRanges();
    sel.addRange(range);

    return false;
  }
};
