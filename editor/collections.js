var dataFolder = "../collections/";
var list={children:[]};

function load(uid){
  var xhttp = new XMLHttpRequest();
  xhttp.open("GET", `${dataFolder}${uid}.col`, false);
  xhttp.send();

  var data = xhttp.responseText;
  data = data.split("\n");
  if(data[data.length-1]=="") data.pop();
  // console.log(`[${data[data.length-1]}]`);
  return data;
}

window.onload = function(){
  list.children = load("list");
  // if(!list.children[list.children.lenght-1]) list.children.pop(); // empty line at the end
  for(i in list.children){
    list.children[i]={
      type:"collection",
      uid:list.children[i].split(":")[0],
      title:list.children[i].split(":")[1]
    };
  }
  output.innerHTML = render(list.children, 0);
};

function render(obj, a){
  var HTML = "";

  // step through list recursively
  for(i in obj){
    HTML += obj[i].type == "collection" ? addCollection(obj[i], `${a},${i}`) : "";
    HTML += obj[i].type == "heading" ? addHeading(obj[i], `${a},${i}`) : "";
    HTML += obj[i].type == "song" ? addSong(obj[i], `${a},${i}`) : "";
    if(obj[i].open) HTML += render(obj[i].children, `${a},${i}`);
  }
  return HTML;
}

function append(address){
  a = address.split(",");
  obj = list;
  for(i=1; i<a.length; i++){
    obj = obj.children[a[i]];
    if(!obj.children) populate(obj, load(obj.uid));
    if(i==a.length-1) obj.open=!obj.open;
  }
  output.innerHTML = render(list.children, 0);
}

function addCollection(obj, address){
  if(!obj.language) obj.language = "ben";
  return `<div class=item onclick="append('${address}')">`
    +`${indent(address)} ${obj.open?'▼':'►'} &nbsp; ${transliterate(obj.title, obj.language)}`
    +` – <small>[${obj.uid}]</small></div>`;
}

function addHeading(obj, address){
  if(!obj.language) obj.language = "ben";
  return `<div class=heading>`
    +`${indent(address)} <b>${transliterate(obj.title, obj.language)}</b>`
    +` – <small>[${obj.uid}]</small></div>`;
}

function addSong(obj, address){
  if(!obj.language) obj.language = "ben";
  return `<div class=item onclick="alert('${obj.uid}')">`
    +`${indent(address)} • &nbsp; ${transliterate(obj.title, obj.language)}`
    +` – <small>[${obj.uid}]</small></div>`;
}

function indent(a){
  return `<div style="width:${(a.split(",").length-2)*60}px;float:left"> </div>`;
}

// parse the collections file and populate the object
function populate(obj, lines){
  obj.open=true;

  var ind=0;
  var oind=0;
  var newObj = ["["];

  function closeBracket(comma){
    oind=ind;
    if(!comma) ind=0;
    else ind=(lines[i][0]=="\t") + (lines[i][1]=="\t");
    for(var j=0; j<=1; j++){
      if(ind<oind-j){
        last=newObj.length-1;
        newObj[last]=newObj[last].replace(/,$/,'');
        newObj.push(`${"".padStart(oind*2-j*2," ")}]}${comma}`);
      }
    }
  }

  for(i in lines){
    l=lines[i].replace(/\t/g,"");
    if(l[0]=="*") type = "heading";
    else type = isNaN(l[0]) ? "song":"collection";
    ls = l.split(":");
    uid = ls[0];
    title = ls[1];
    closeBracket(",");
    if(type=="collection"){ c=0; children=', "children":['; }
    else children="},";
    newObj.push(`${"".padStart(ind*2+2," ")}{"type":"${type}", "uid":"${uid}", "title":"${title}"${children}`);
  }
  closeBracket("");
  last=newObj.length-1;
  newObj[last]=newObj[last].replace(/,$/,'');
  newObj.push("]");
  // console.log(newObj.join("\n"));
  obj.children=JSON.parse(newObj.join("\n"));
}
