DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR

clear
if [ "$(git branch)" == "* master" ] ; then
  echo "what would you like to call your git branch?"
  read b
  git checkout -b $b
fi

a="."
if [ "$1" != "" ] ; then a="$1" ; fi
# echo "git add $a"
git add $a

echo "what would you like to mention about this commit?"
read m
# echo "git commit -m \"$m\""
git commit -m "$m"
echo -n "would you like to publish this to gitlab.com [y/n]? "
read yn
if [ "$yn" = "y" ] ; then git push ; fi
